<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Form_sasaran_mutu extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(['sql', 'm_custom']);
	}

	public function index()
	{
		$year = date("Y");
		if ($this->input->get()) {
			$year = $this->input->get('year');
		}
		$data['year'] = $year;
		$where = ['tahun' => $year];
        
        $sasaran_mutu = $this->sql->get('tbl_sasaran_mutu', $where)->result_array();
        $data_sasaran_mutu = [];
        foreach($sasaran_mutu as $row){
            $data_sasaran_mutu[$row['layanan_id']] = $row['target'];
        }
        $data['sasaran_mutu'] = $data_sasaran_mutu;

        $data['year'] = $year;
		$detail = $this->m_custom->sasaran_mutu($where)->result_array();
        $data['layanan'] = $this->sql->get('tbl_layanan', ["slug IN('pelatihan','rbpi','teknologi-proses')" => NULL])->result_array();
        

		$data_detail = [];
		foreach ($detail as $row) {
			$data_detail[$row['layanan_id']][$row['bulan']] = $row['hasil'];
		}

		$data['detail'] = $data_detail;

		$data['subview'] = "form_sasaran_mutu/data";
		$data['site_title'] = "Form sasaran_mutu";
		$this->load->view('index', $data);
	}

	function get()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

            $where = ['tahun' => $post['tahun'], 'layanan_id' => $post['layanan']];
            $check_header = $this->sql->get('tbl_sasaran_mutu', $where)->num_rows();
            if($check_header == 0){
                $data = [
                    'tahun' => $post['tahun'],
                    'layanan_id' => $post['layanan']
                ];
                $this->sql->create('tbl_sasaran_mutu', $data);
            }

            $header = $this->sql->get('tbl_sasaran_mutu', $where)->row_array();

            $where = [
                'sasaran_mutu_id' => $header['sasaran_mutu_id'],
                'bulan' => $post['bulan']
            ];

			$res = $this->sql->get('tbl_sasaran_mutu_detail', $where);
			$data['hasil'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['hasil'] = $result['hasil'];
			}
			$data['tahun'] = $post['tahun'];
			$data['bulan'] = $post['bulan'];
			$data['layanan'] = $post['layanan'];
			$this->load->view('form_sasaran_mutu/get', $data);
		} else {
			echo "error";
		}
	}

	function get_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
                'layanan_id' => $post['layanan']
			];

			$res = $this->sql->get("tbl_sasaran_mutu", $where);
			$data['target'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['target'] = $result['target'];
			}
			$data['tahun'] = $post['tahun'];
			$data['layanan'] = $post['layanan'];

			$this->load->view('form_sasaran_mutu/get_target', $data);
		} else {
			echo "error";
		}
	}

	function update()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

            $header = $this->sql->get('tbl_sasaran_mutu', ['tahun' => $post['tahun'], 'layanan_id' => $post['layanan_id']])->row_array();

			$where = [
				'sasaran_mutu_id' => $header['sasaran_mutu_id'],
				'bulan' => $post['bulan']
			];

			$check = $this->sql->get('tbl_sasaran_mutu_detail', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->update('tbl_sasaran_mutu_detail', $form_data, $where);
			} else {
				$form_data = [
					'sasaran_mutu_id' => $header['sasaran_mutu_id'],
					'bulan' => $post['bulan'],
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->create('tbl_sasaran_mutu_detail', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function update_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
                'layanan_id' => $post['layanan_id']
			];

			$check = $this->sql->get('tbl_sasaran_mutu', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'target' => $post['target']
				];
				$update = $this->sql->update('tbl_sasaran_mutu', $form_data, $where);
			} else {
				$form_data = [
					'tahun' => $post['tahun'],
                    'layanan_id' => $post['layanan_id'],
					'target' => $post['target']
				];
				$update = $this->sql->create('tbl_sasaran_mutu', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
