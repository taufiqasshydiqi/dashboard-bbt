<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Form_wo extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(['sql', 'm_custom']);
	}

	public function index()
	{
		$year = date("Y");
		if ($this->input->get()) {
			$year = $this->input->get('year');
		}
		$data['year'] = $year;
		$where = ['tahun' => $year];
        
        $wo = $this->sql->get('tbl_wo', $where)->result_array();
        $data_wo = [];
        foreach($wo as $row){
            $data_wo[$row['layanan_id']] = $row['target'];
        }
        $data['wo'] = $data_wo;

        $data['year'] = $year;
		$detail = $this->m_custom->wo($where)->result_array();
        $data['layanan'] = $this->sql->get('tbl_layanan', ["slug IN('pengujian','lingkungan','kalibrasi')" => NULL])->result_array();
        

		$data_detail = [];
		foreach ($detail as $row) {
			$data_detail[$row['layanan_id']][$row['bulan']] = $row['hasil'];
		}

		$data['detail'] = $data_detail;

		$data['subview'] = "form_wo/data";
		$data['site_title'] = "Form wo";
		$this->load->view('index', $data);
	}

	function get()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

            $where = ['tahun' => $post['tahun'], 'layanan_id' => $post['layanan']];
            $check_header = $this->sql->get('tbl_wo', $where)->num_rows();
            if($check_header == 0){
                $data = [
                    'tahun' => $post['tahun'],
                    'layanan_id' => $post['layanan']
                ];
                $this->sql->create('tbl_wo', $data);
            }

            $header = $this->sql->get('tbl_wo', $where)->row_array();

            $where = [
                'wo_id' => $header['wo_id'],
                'bulan' => $post['bulan']
            ];

			$res = $this->sql->get('tbl_wo_detail', $where);
			$data['hasil'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['hasil'] = $result['hasil'];
			}
			$data['tahun'] = $post['tahun'];
			$data['bulan'] = $post['bulan'];
			$data['layanan'] = $post['layanan'];
			$this->load->view('form_wo/get', $data);
		} else {
			echo "error";
		}
	}

	function get_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
                'layanan_id' => $post['layanan']
			];

			$res = $this->sql->get("tbl_wo", $where);
			$data['target'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['target'] = $result['target'];
			}
			$data['tahun'] = $post['tahun'];
			$data['layanan'] = $post['layanan'];

			$this->load->view('form_wo/get_target', $data);
		} else {
			echo "error";
		}
	}

	function update()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

            $header = $this->sql->get('tbl_wo', ['tahun' => $post['tahun'], 'layanan_id' => $post['layanan_id']])->row_array();

			$where = [
				'wo_id' => $header['wo_id'],
				'bulan' => $post['bulan']
			];

			$check = $this->sql->get('tbl_wo_detail', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->update('tbl_wo_detail', $form_data, $where);
			} else {
				$form_data = [
					'wo_id' => $header['wo_id'],
					'bulan' => $post['bulan'],
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->create('tbl_wo_detail', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function update_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
                'layanan_id' => $post['layanan_id']
			];

			$check = $this->sql->get('tbl_wo', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'target' => $post['target']
				];
				$update = $this->sql->update('tbl_wo', $form_data, $where);
			} else {
				$form_data = [
					'tahun' => $post['tahun'],
                    'layanan_id' => $post['layanan_id'],
					'target' => $post['target']
				];
				$update = $this->sql->create('tbl_wo', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
