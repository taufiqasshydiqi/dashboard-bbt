<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Form_pnbp extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(['sql', 'm_custom']);
	}

	public function index()
	{
		$year = date("Y");
		if ($this->input->get()) {
			$year = $this->input->get('year');
		}
		$data['year'] = $year;
		$where = [
			'tahun' => $year
		];
		$data['pnbp'] = $this->m_custom->pnbp(null, $year)->result_array();
		$detail = $this->sql->get('tbl_pnbp_detail')->result_array();

		$data_detail = [];
		foreach ($detail as $row) {
			$data_detail[$row['pnbp_id']][$row['bulan']] = $row['hasil'];
		}

		$data['detail'] = $data_detail;

		$data['subview'] = "form_pnbp/data";
		$data['site_title'] = "Form PNBP";
		$this->load->view('index', $data);
	}

	function get()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'bulan' => $post['bulan'],
				'tahun' => $post['tahun'],
				'a.pnbp_id' => $post['id']
			];

			$res = $this->m_custom->pnbp_detail($where);
			$data['hasil'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['hasil'] = $result['hasil'];
			}
			$data['tahun'] = $post['tahun'];
			$data['bulan'] = $post['bulan'];
			$data['id'] = $post['id'];
			$data['layanan'] = $post['layanan'];

			$this->load->view('form_pnbp/get', $data);
		} else {
			echo "error";
		}
	}

	function get_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
				'layanan_id' => $post['layanan']
			];

			$res = $this->sql->get("tbl_pnbp", $where);
			$data['target'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['target'] = $result['target'];
			}
			$data['tahun'] = $post['tahun'];
			$data['layanan'] = $post['layanan'];

			$this->load->view('form_pnbp/get_target', $data);
		} else {
			echo "error";
		}
	}

	function update($id = null)
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			if (!isset($id)) {
				$form_data = [
					'layanan_id' => $post['layanan'],
					'tahun' => $post['tahun']
				];

				$id = $this->sql->create('tbl_pnbp', $form_data);
			}

			$where = [
				'pnbp_id' => $id,
				'bulan' => $post['bulan']
			];

			$check = $this->sql->get('tbl_pnbp_detail', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->update('tbl_pnbp_detail', $form_data, $where);
			} else {
				$form_data = [
					'pnbp_id' => $id,
					'bulan' => $post['bulan'],
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->create('tbl_pnbp_detail', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function update_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
				'layanan_id' => $post['layanan']
			];

			$check = $this->sql->get('tbl_pnbp', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'target' => $post['target']
				];
				$update = $this->sql->update('tbl_pnbp', $form_data, $where);
			} else {
				$form_data = [
					'tahun' => $post['tahun'],
					'layanan_id' => $post['layanan'],
					'target' => $post['target']
				];
				$update = $this->sql->create('tbl_pnbp', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
