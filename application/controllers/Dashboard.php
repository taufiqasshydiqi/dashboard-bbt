<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(['sql', 'm_custom']);
	}

	public function index()
	{
		$year = date('Y');
		$month = date('n');
		if ($this->input->get()) {
			$year = $this->input->get('year');
			$month = $this->input->get('month');
		}
		$data['year'] = $year;
		$data['month'] = $month;

		$pnbp_header = $this->m_custom->pnbp_dashboard($year, $month)->result_array();
		$pnbp = null;
		$target_pnbp = 0;
		$realisasi_pnbp = 0;
		foreach ($pnbp_header as $row) {
			$hasil = (isset($row['hasil'])) ? $row['hasil'] : 0;
			$target = (isset($row['target'])) ? $row['target'] : 0;
			$pnbp[$row['slug']] = $hasil;
			$target_pnbp += $target;
			$realisasi_pnbp += $hasil;
		}
		$data['target_pnbp'] = $target_pnbp;
		$data['realisasi_pnbp'] = $realisasi_pnbp;
		$data['percentage_pnbp'] = ($realisasi_pnbp / $target_pnbp) * 100;
		$data['sisa_pnbp'] = $target_pnbp - $realisasi_pnbp;
		$data['pnbp'] = $pnbp;

		$where = [
			'tahun' => $year
		];
		$header_rm = $this->sql->get('tbl_anggaran_rm', $where)->row_array();
		if (count($header_rm) > 0) {
			$data['target_rm'] = $header_rm['target'];
			$anggaran_rm_id = $header_rm['anggaran_rm_id'];
		} else {
			$data['target_rm'] = 0;
			$anggaran_rm_id = 0;
		}
		$where = [
			'anggaran_rm_id' => $anggaran_rm_id,
			'bulan' => $month
		];
		$detail_rm = $this->sql->get('tbl_anggaran_rm_detail', $where);
		if ($detail_rm->num_rows() > 0) {
			$detail_rm = $detail_rm->row_array();
			$data['realisasi_rm'] = $detail_rm['hasil'];
		} else {
			$data['realisasi_rm'] = 0;
		}
		$data['percentage_rm'] = ($data['realisasi_rm'] / $data['target_rm']) * 100;

		$where = ['tahun' => $year];
		$ikm = $this->sql->get('tbl_ikm', $where)->row_array();
		if (count($ikm) > 0) {
			$data['target_ikm'] = $ikm['target'];
			$ikm_id = $ikm['ikm_id'];
		} else {
			$data['target_ikm'] = 0;
			$ikm_id = 0;
		}

		$where = [
			'ikm_id' => $ikm_id,
			'bulan' => $month
		];
		$ikm_detail = $this->sql->get('tbl_ikm_detail', $where);
		if ($ikm_detail->num_rows() > 0) {
			$ikm_detail =  $ikm_detail->row_array();
			$data['realisasi_ikm'] = $ikm_detail['hasil'];
		} else {
			$data['realisasi_ikm'] = 0;
		}

		$where = ['tahun' => $year];
		$ipk = $this->sql->get('tbl_ipk', $where)->row_array();
		if (count($ipk) > 0) {
			$data['target_ipk'] = $ipk['target'];
			$ipk_id = $ipk['ipk_id'];
		} else {
			$data['target_ipk'] = 0;
			$ipk_id = 0;
		}

		$where = [
			'ipk_id' => $ipk_id,
			'bulan' => $month
		];
		$ipk_detail = $this->sql->get('tbl_ipk_detail', $where);
		if ($ipk_detail->num_rows() > 0) {
			$ipk_detail =  $ipk_detail->row_array();
			$data['realisasi_ipk'] = $ipk_detail['hasil'];
		} else {
			$data['realisasi_ipk'] = 0;
		}

		$where = ['tahun' => $year];
		$pemenuhan_standard = $this->sql->get('tbl_pemenuhan_standard', $where)->row_array();
		if (count($pemenuhan_standard) > 0) {
			$data['target_pemenuhan_standard'] = $pemenuhan_standard['target'];
			$pemenuhan_standard_id = $pemenuhan_standard['pemenuhan_standard_id'];
		} else {
			$data['target_pemenuhan_standard'] = 0;
			$pemenuhan_standard_id = 0;
		}

		$where = [
			'pemenuhan_standard_id' => $pemenuhan_standard_id,
			'bulan' => $month
		];
		$pemenuhan_standard_detail = $this->sql->get('tbl_pemenuhan_standard_detail', $where);
		if ($pemenuhan_standard_detail->num_rows() > 0) {
			$pemenuhan_standard_detail =  $pemenuhan_standard_detail->row_array();
			$data['realisasi_pemenuhan_standard'] = $pemenuhan_standard_detail['hasil'];
		} else {
			$data['realisasi_pemenuhan_standard'] = 0;
		}

		$where = ['tahun' => $year];
		$persentase_komplain = $this->sql->get('tbl_persentase_komplain', $where)->row_array();
		if (count($persentase_komplain) > 0) {
			$data['target_persentase_komplain'] = $persentase_komplain['target'];
			$persentase_komplain_id = $persentase_komplain['persentase_komplain_id'];
		} else {
			$data['target_persentase_komplain'] = 0;
			$persentase_komplain_id = 0;
		}

		$where = [
			'persentase_komplain_id' => $persentase_komplain_id,
			'bulan' => $month
		];
		$persentase_komplain_detail = $this->sql->get('tbl_persentase_komplain_detail', $where);
		if ($persentase_komplain_detail->num_rows() > 0) {
			$persentase_komplain_detail =  $persentase_komplain_detail->row_array();
			$data['realisasi_persentase_komplain'] = $persentase_komplain_detail['hasil'];
		} else {
			$data['realisasi_persentase_komplain'] = 0;
		}

		$where = ['tahun' => $year];
		$wo = $this->sql->get('tbl_wo', $where)->result_array();
		$data['realisasi_wo'] = 0;
		if (count($wo) > 0) {
			foreach ($wo as $row) {
				$where = [
					'wo_id' => $row['wo_id'],
					'bulan' => $month
				];
				$wo_detail = $this->sql->get('tbl_wo_detail', $where);
				if ($wo_detail->num_rows() > 0) {
					$wo_detail =  $wo_detail->row_array();
					$data['realisasi_wo'] += $wo_detail['hasil'];
				}
			}
		}

		$where = ['tahun' => $year];
		$customer = $this->sql->get('tbl_customer', $where)->result_array();
		$data['realisasi_customer'] = 0;
		if (count($customer) > 0) {
			foreach ($customer as $row) {
				$where = [
					'customer_id' => $row['customer_id'],
					'bulan' => $month
				];
				$customer_detail = $this->sql->get('tbl_customer_detail', $where);
				if ($customer_detail->num_rows() > 0) {
					$customer_detail =  $customer_detail->row_array();
					$data['realisasi_customer'] += $customer_detail['hasil'];
				}
			}
		}

		$data['subview'] = "dashboard/dashboard";
		$data['site_title'] = "Dashboard";
		$this->load->view('index', $data);
	}

	public function dashboard_emon()
	{
		$data['subview'] = "dashboard/dashboard_emon";
		$data['site_title'] = "Dashboard E-Mon";
		$this->load->view('index', $data);
	}

	public function dashboard_sdm()
	{
		$data['history'] = $this->m_custom->history();
		$data['subview'] = "dashboard/dashboard_sdm";
		$data['site_title'] = "Dashboard SDM";
		$this->load->view('index', $data);
	}

	public function detail_pengujian()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$data['kategori'] = $post['kategori'];
			$layanan = $this->sql->get('tbl_layanan', ['slug' => $post['kategori']])->row_array();

			$sample_header = $this->sql->get('tbl_sample', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($sample_header->num_rows() > 0) {
				$sample_header = $sample_header->row_array();
				$sample_id = $sample_header['sample_id'];
			} else {
				$sample_id = 0;
			}
			if ($sample_id == 0) {
				$data['sample'] = 0;
			} else {
				$detail = $this->sql->get('tbl_sample_detail', ['bulan' => $post['month'], 'sample_id' => $sample_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['sample'] = $detail['hasil'];
				} else {
					$data['sample'] = 0;
				}
			}

			$wo_header = $this->sql->get('tbl_wo', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($wo_header->num_rows() > 0) {
				$wo_header = $wo_header->row_array();
				$wo_id = $wo_header['wo_id'];
			} else {
				$wo_id = 0;
			}
			if ($wo_id == 0) {
				$data['wo'] = 0;
			} else {
				$detail = $this->sql->get('tbl_wo_detail', ['bulan' => $post['month'], 'wo_id' => $wo_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['wo'] = $detail['hasil'];
				} else {
					$data['wo'] = 0;
				}
			}

			$ketepatan_waktu_header = $this->sql->get('tbl_ketepatan_waktu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($ketepatan_waktu_header->num_rows() > 0) {
				$ketepatan_waktu_header = $ketepatan_waktu_header->row_array();
				$ketepatan_waktu_id = $ketepatan_waktu_header['ketepatan_waktu_id'];
			} else {
				$ketepatan_waktu_id = 0;
			}
			if ($ketepatan_waktu_id == 0) {
				$data['ketepatan_waktu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_ketepatan_waktu_detail', ['bulan' => $post['month'], 'ketepatan_waktu_id' => $ketepatan_waktu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['ketepatan_waktu'] = $detail['hasil'];
				} else {
					$data['ketepatan_waktu'] = 0;
				}
			}



			$customer_header = $this->sql->get('tbl_customer', ['tahun' => $post['year'], "customer_type_id IN(SELECT customer_type_id FROM tbl_customer_type WHERE layanan_id = '" . $layanan['layanan_id'] . "')" => NULL]);
			if ($customer_header->num_rows() > 0) {
				$customer_header = $customer_header->result_array();
				foreach ($customer_header as $row) {
					$customer_id = $row['customer_id'];
					$detail = $this->sql->get('tbl_customer_detail', ['bulan' => $post['month'], 'customer_id' => $customer_id]);
					if ($detail->num_rows() > 0) {
						$detail = $detail->row_array();

						if ($row['customer_type_id'] == 1) {
							$data['customer_industri'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 2) {
							$data['customer_pemerintah'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 3) {
							$data['customer_ikm'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 12) {
							$data['customer_lainnya'] = $detail['hasil'];
						}
					} else {
						if ($row['customer_type_id'] == 1) {
							$data['customer_industri'] = 0;
						} else if ($row['customer_type_id'] == 2) {
							$data['customer_pemerintah'] = 0;
						} else if ($row['customer_type_id'] == 3) {
							$data['customer_ikm'] = 0;
						} else if ($row['customer_type_id'] == 12) {
							$data['customer_lainnya'] = 0;
						}
					}
				}

				if (!isset($data['customer_industri'])) {
					$data['customer_industri'] = 0;
				}
				if (!isset($data['customer_pemerintah'])) {
					$data['customer_pemerintah'] = 0;
				}
				if (!isset($data['customer_ikm'])) {
					$data['customer_ikm'] = 0;
				}
				if (!isset($data['customer_lainnya'])) {
					$data['customer_lainnya'] = 0;
				}
			} else {
				$data['customer_industri'] = 0;
				$data['customer_pemerintah'] = 0;
				$data['customer_ikm'] = 0;
				$data['customer_lainnya'] = 0;
			}
			$this->load->view('dashboard/detail_pengujian', $data);
		} else {
			echo "error";
		}
	}

	public function detail_lingkungan()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$data['kategori'] = $post['kategori'];
			$layanan = $this->sql->get('tbl_layanan', ['slug' => $post['kategori']])->row_array();

			$sample_header = $this->sql->get('tbl_sample', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($sample_header->num_rows() > 0) {
				$sample_header = $sample_header->row_array();
				$sample_id = $sample_header['sample_id'];
			} else {
				$sample_id = 0;
			}
			if ($sample_id == 0) {
				$data['sample'] = 0;
			} else {
				$detail = $this->sql->get('tbl_sample_detail', ['bulan' => $post['month'], 'sample_id' => $sample_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['sample'] = $detail['hasil'];
				} else {
					$data['sample'] = 0;
				}
			}

			$wo_header = $this->sql->get('tbl_wo', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($wo_header->num_rows() > 0) {
				$wo_header = $wo_header->row_array();
				$wo_id = $wo_header['wo_id'];
			} else {
				$wo_id = 0;
			}
			if ($wo_id == 0) {
				$data['wo'] = 0;
			} else {
				$detail = $this->sql->get('tbl_wo_detail', ['bulan' => $post['month'], 'wo_id' => $wo_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['wo'] = $detail['hasil'];
				} else {
					$data['wo'] = 0;
				}
			}

			$ketepatan_waktu_header = $this->sql->get('tbl_ketepatan_waktu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($ketepatan_waktu_header->num_rows() > 0) {
				$ketepatan_waktu_header = $ketepatan_waktu_header->row_array();
				$ketepatan_waktu_id = $ketepatan_waktu_header['ketepatan_waktu_id'];
			} else {
				$ketepatan_waktu_id = 0;
			}
			if ($ketepatan_waktu_id == 0) {
				$data['ketepatan_waktu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_ketepatan_waktu_detail', ['bulan' => $post['month'], 'ketepatan_waktu_id' => $ketepatan_waktu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['ketepatan_waktu'] = $detail['hasil'];
				} else {
					$data['ketepatan_waktu'] = 0;
				}
			}

			$customer_header = $this->sql->get('tbl_customer', ['tahun' => $post['year'], "customer_type_id IN(SELECT customer_type_id FROM tbl_customer_type WHERE layanan_id = '" . $layanan['layanan_id'] . "')" => NULL]);
			if ($customer_header->num_rows() > 0) {
				$customer_header = $customer_header->result_array();
				foreach ($customer_header as $row) {
					$customer_id = $row['customer_id'];
					$detail = $this->sql->get('tbl_customer_detail', ['bulan' => $post['month'], 'customer_id' => $customer_id]);
					if ($detail->num_rows() > 0) {
						$detail = $detail->row_array();

						if ($row['customer_type_id'] == 13) {
							$data['customer_industri'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 14) {
							$data['customer_pemerintah'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 15) {
							$data['customer_ikm'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 16) {
							$data['customer_lainnya'] = $detail['hasil'];
						}
					} else {
						if ($row['customer_type_id'] == 13) {
							$data['customer_industri'] = 0;
						} else if ($row['customer_type_id'] == 14) {
							$data['customer_pemerintah'] = 0;
						} else if ($row['customer_type_id'] == 15) {
							$data['customer_ikm'] = 0;
						} else if ($row['customer_type_id'] == 16) {
							$data['customer_lainnya'] = 0;
						}
					}
				}

				if (!isset($data['customer_industri'])) {
					$data['customer_industri'] = 0;
				}
				if (!isset($data['customer_pemerintah'])) {
					$data['customer_pemerintah'] = 0;
				}
				if (!isset($data['customer_ikm'])) {
					$data['customer_ikm'] = 0;
				}
				if (!isset($data['customer_lainnya'])) {
					$data['customer_lainnya'] = 0;
				}
			} else {
				$data['customer_industri'] = 0;
				$data['customer_pemerintah'] = 0;
				$data['customer_ikm'] = 0;
				$data['customer_lainnya'] = 0;
			}
			$this->load->view('dashboard/detail_lingkungan', $data);
		} else {
			echo "error";
		}
	}

	public function detail_kalibrasi()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$data['kategori'] = $post['kategori'];
			$layanan = $this->sql->get('tbl_layanan', ['slug' => $post['kategori']])->row_array();

			$sample_header = $this->sql->get('tbl_sample', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($sample_header->num_rows() > 0) {
				$sample_header = $sample_header->row_array();
				$sample_id = $sample_header['sample_id'];
			} else {
				$sample_id = 0;
			}
			if ($sample_id == 0) {
				$data['sample'] = 0;
			} else {
				$detail = $this->sql->get('tbl_sample_detail', ['bulan' => $post['month'], 'sample_id' => $sample_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['sample'] = $detail['hasil'];
				} else {
					$data['sample'] = 0;
				}
			}

			$wo_header = $this->sql->get('tbl_wo', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($wo_header->num_rows() > 0) {
				$wo_header = $wo_header->row_array();
				$wo_id = $wo_header['wo_id'];
			} else {
				$wo_id = 0;
			}
			if ($wo_id == 0) {
				$data['wo'] = 0;
			} else {
				$detail = $this->sql->get('tbl_wo_detail', ['bulan' => $post['month'], 'wo_id' => $wo_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['wo'] = $detail['hasil'];
				} else {
					$data['wo'] = 0;
				}
			}

			$ketepatan_waktu_header = $this->sql->get('tbl_ketepatan_waktu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($ketepatan_waktu_header->num_rows() > 0) {
				$ketepatan_waktu_header = $ketepatan_waktu_header->row_array();
				$ketepatan_waktu_id = $ketepatan_waktu_header['ketepatan_waktu_id'];
			} else {
				$ketepatan_waktu_id = 0;
			}
			if ($ketepatan_waktu_id == 0) {
				$data['ketepatan_waktu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_ketepatan_waktu_detail', ['bulan' => $post['month'], 'ketepatan_waktu_id' => $ketepatan_waktu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['ketepatan_waktu'] = $detail['hasil'];
				} else {
					$data['ketepatan_waktu'] = 0;
				}
			}

			$customer_header = $this->sql->get('tbl_customer', ['tahun' => $post['year'], "customer_type_id IN(SELECT customer_type_id FROM tbl_customer_type WHERE layanan_id = '" . $layanan['layanan_id'] . "')" => NULL]);
			if ($customer_header->num_rows() > 0) {
				$customer_header = $customer_header->result_array();
				foreach ($customer_header as $row) {
					$customer_id = $row['customer_id'];
					$detail = $this->sql->get('tbl_customer_detail', ['bulan' => $post['month'], 'customer_id' => $customer_id]);
					if ($detail->num_rows() > 0) {
						$detail = $detail->row_array();

						if ($row['customer_type_id'] == 20) {
							$data['customer_industri'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 21) {
							$data['customer_pemerintah'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 22) {
							$data['customer_ikm'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 23) {
							$data['customer_lainnya'] = $detail['hasil'];
						}
					} else {
						if ($row['customer_type_id'] == 20) {
							$data['customer_industri'] = 0;
						} else if ($row['customer_type_id'] == 21) {
							$data['customer_pemerintah'] = 0;
						} else if ($row['customer_type_id'] == 22) {
							$data['customer_ikm'] = 0;
						} else if ($row['customer_type_id'] == 23) {
							$data['customer_lainnya'] = 0;
						}
					}
				}

				if (!isset($data['customer_industri'])) {
					$data['customer_industri'] = 0;
				}
				if (!isset($data['customer_pemerintah'])) {
					$data['customer_pemerintah'] = 0;
				}
				if (!isset($data['customer_ikm'])) {
					$data['customer_ikm'] = 0;
				}
				if (!isset($data['customer_lainnya'])) {
					$data['customer_lainnya'] = 0;
				}
			} else {
				$data['customer_industri'] = 0;
				$data['customer_pemerintah'] = 0;
				$data['customer_ikm'] = 0;
				$data['customer_lainnya'] = 0;
			}
			$this->load->view('dashboard/detail_kalibrasi', $data);
		} else {
			echo "error";
		}
	}

	public function detail_sertifikasi_produk()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$data['kategori'] = $post['kategori'];
			$layanan = $this->sql->get('tbl_layanan', ['slug' => $post['kategori']])->row_array();

			$ketepatan_waktu_header = $this->sql->get('tbl_ketepatan_waktu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($ketepatan_waktu_header->num_rows() > 0) {
				$ketepatan_waktu_header = $ketepatan_waktu_header->row_array();
				$ketepatan_waktu_id = $ketepatan_waktu_header['ketepatan_waktu_id'];
			} else {
				$ketepatan_waktu_id = 0;
			}
			if ($ketepatan_waktu_id == 0) {
				$data['ketepatan_waktu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_ketepatan_waktu_detail', ['bulan' => $post['month'], 'ketepatan_waktu_id' => $ketepatan_waktu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['ketepatan_waktu'] = $detail['hasil'];
				} else {
					$data['ketepatan_waktu'] = 0;
				}
			}

			$customer_header = $this->sql->get('tbl_customer', ['tahun' => $post['year'], "customer_type_id IN(SELECT customer_type_id FROM tbl_customer_type WHERE layanan_id = '" . $layanan['layanan_id'] . "')" => NULL]);
			if ($customer_header->num_rows() > 0) {
				$customer_header = $customer_header->result_array();
				foreach ($customer_header as $row) {
					$customer_id = $row['customer_id'];
					$detail = $this->sql->get('tbl_customer_detail', ['bulan' => $post['month'], 'customer_id' => $customer_id]);
					if ($detail->num_rows() > 0) {
						$detail = $detail->row_array();

						if ($row['customer_type_id'] == 4) {
							$data['customer_sni_sukarela'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 5) {
							$data['customer_sni_wajib'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 24) {
							$data['customer_industri'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 25) {
							$data['customer_ikm'] = $detail['hasil'];
						}
					} else {
						if ($row['customer_type_id'] == 20) {
							$data['customer_sni_sukarela'] = 0;
						} else if ($row['customer_type_id'] == 21) {
							$data['customer_sni_wajib'] = 0;
						} else if ($row['customer_type_id'] == 22) {
							$data['customer_industri'] = 0;
						} else if ($row['customer_type_id'] == 23) {
							$data['customer_ikm'] = 0;
						}
					}
				}

				if (!isset($data['customer_sni_sukarela'])) {
					$data['customer_sni_sukarela'] = 0;
				}
				if (!isset($data['customer_sni_wajib'])) {
					$data['customer_sni_wajib'] = 0;
				}
				if (!isset($data['customer_industri'])) {
					$data['customer_industri'] = 0;
				}
				if (!isset($data['customer_ikm'])) {
					$data['customer_ikm'] = 0;
				}
			} else {
				$data['customer_sni_sukarela'] = 0;
				$data['customer_sni_wajib'] = 0;
				$data['customer_industri'] = 0;
				$data['customer_ikm'] = 0;
			}
			$this->load->view('dashboard/detail_sertifikasi_produk', $data);
		} else {
			echo "error";
		}
	}

	public function detail_sertifikasi_sistem_mutu()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$data['kategori'] = $post['kategori'];
			$layanan = $this->sql->get('tbl_layanan', ['slug' => $post['kategori']])->row_array();

			$ketepatan_waktu_header = $this->sql->get('tbl_ketepatan_waktu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($ketepatan_waktu_header->num_rows() > 0) {
				$ketepatan_waktu_header = $ketepatan_waktu_header->row_array();
				$ketepatan_waktu_id = $ketepatan_waktu_header['ketepatan_waktu_id'];
			} else {
				$ketepatan_waktu_id = 0;
			}
			if ($ketepatan_waktu_id == 0) {
				$data['ketepatan_waktu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_ketepatan_waktu_detail', ['bulan' => $post['month'], 'ketepatan_waktu_id' => $ketepatan_waktu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['ketepatan_waktu'] = $detail['hasil'];
				} else {
					$data['ketepatan_waktu'] = 0;
				}
			}

			$customer_header = $this->sql->get('tbl_customer', ['tahun' => $post['year'], "customer_type_id IN(SELECT customer_type_id FROM tbl_customer_type WHERE layanan_id = '" . $layanan['layanan_id'] . "')" => NULL]);
			if ($customer_header->num_rows() > 0) {
				$customer_header = $customer_header->result_array();
				foreach ($customer_header as $row) {
					$customer_id = $row['customer_id'];
					$detail = $this->sql->get('tbl_customer_detail', ['bulan' => $post['month'], 'customer_id' => $customer_id]);
					if ($detail->num_rows() > 0) {
						$detail = $detail->row_array();

						if ($row['customer_type_id'] == 6) {
							$data['customer_sistem_mutu'] = $detail['hasil'];
						}
					} else {
						if ($row['customer_type_id'] == 6) {
							$data['customer_sistem_mutu'] = 0;
						}
					}
				}

				if (!isset($data['customer_sistem_mutu'])) {
					$data['customer_sistem_mutu'] = 0;
				}
			} else {
				$data['customer_sistem_mutu'] = 0;
			}
			$this->load->view('dashboard/detail_sertifikasi_sistem_mutu', $data);
		} else {
			echo "error";
		}
	}

	public function detail_konsultasi()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$data['kategori'] = $post['kategori'];
			$layanan = $this->sql->get('tbl_layanan', ['slug' => $post['kategori']])->row_array();

			$ketepatan_waktu_header = $this->sql->get('tbl_ketepatan_waktu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($ketepatan_waktu_header->num_rows() > 0) {
				$ketepatan_waktu_header = $ketepatan_waktu_header->row_array();
				$ketepatan_waktu_id = $ketepatan_waktu_header['ketepatan_waktu_id'];
			} else {
				$ketepatan_waktu_id = 0;
			}
			if ($ketepatan_waktu_id == 0) {
				$data['ketepatan_waktu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_ketepatan_waktu_detail', ['bulan' => $post['month'], 'ketepatan_waktu_id' => $ketepatan_waktu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['ketepatan_waktu'] = $detail['hasil'];
				} else {
					$data['ketepatan_waktu'] = 0;
				}
			}

			$customer_header = $this->sql->get('tbl_customer', ['tahun' => $post['year'], "customer_type_id IN(SELECT customer_type_id FROM tbl_customer_type WHERE layanan_id = '" . $layanan['layanan_id'] . "')" => NULL]);
			if ($customer_header->num_rows() > 0) {
				$customer_header = $customer_header->result_array();
				foreach ($customer_header as $row) {
					$customer_id = $row['customer_id'];
					$detail = $this->sql->get('tbl_customer_detail', ['bulan' => $post['month'], 'customer_id' => $customer_id]);
					if ($detail->num_rows() > 0) {
						$detail = $detail->row_array();

						if ($row['customer_type_id'] == 7) {
							$data['customer_konsultasi_tpt'] = $detail['hasil'];
						} else if ($row['customer_type_id'] == 8) {
							$data['customer_konsultasi_tkdn'] = $detail['hasil'];
						}
					} else {
						if ($row['customer_type_id'] == 7) {
							$data['customer_konsultasi_tpt'] = 0;
						} else if ($row['customer_type_id'] == 8) {
							$data['customer_konsultasi_tkdn'] = 0;
						}
					}
				}

				if (!isset($data['customer_konsultasi_tpt'])) {
					$data['customer_konsultasi_tpt'] = 0;
				}
				if (!isset($data['customer_konsultasi_tkdn'])) {
					$data['customer_konsultasi_tkdn'] = 0;
				}
			} else {
				$data['customer_konsultasi_tpt'] = 0;
				$data['customer_konsultasi_tkdn'] = 0;
			}
			$this->load->view('dashboard/detail_konsultasi', $data);
		} else {
			echo "error";
		}
	}

	public function detail_pelatihan()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$data['kategori'] = $post['kategori'];
			$layanan = $this->sql->get('tbl_layanan', ['slug' => $post['kategori']])->row_array();

			$sample_header = $this->sql->get('tbl_sample', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($sample_header->num_rows() > 0) {
				$sample_header = $sample_header->row_array();
				$sample_id = $sample_header['sample_id'];
			} else {
				$sample_id = 0;
			}
			if ($sample_id == 0) {
				$data['sample'] = 0;
			} else {
				$detail = $this->sql->get('tbl_sample_detail', ['bulan' => $post['month'], 'sample_id' => $sample_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['sample'] = $detail['hasil'];
				} else {
					$data['sample'] = 0;
				}
			}

			$sasaran_mutu_header = $this->sql->get('tbl_sasaran_mutu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($sasaran_mutu_header->num_rows() > 0) {
				$sasaran_mutu_header = $sasaran_mutu_header->row_array();
				$sasaran_mutu_id = $sasaran_mutu_header['sasaran_mutu_id'];
			} else {
				$sasaran_mutu_id = 0;
			}
			if ($sasaran_mutu_id == 0) {
				$data['sasaran_mutu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_sasaran_mutu_detail', ['bulan' => $post['month'], 'sasaran_mutu_id' => $sasaran_mutu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['sasaran_mutu'] = $detail['hasil'];
				} else {
					$data['sasaran_mutu'] = 0;
				}
			}

			$ketepatan_waktu_header = $this->sql->get('tbl_ketepatan_waktu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($ketepatan_waktu_header->num_rows() > 0) {
				$ketepatan_waktu_header = $ketepatan_waktu_header->row_array();
				$ketepatan_waktu_id = $ketepatan_waktu_header['ketepatan_waktu_id'];
			} else {
				$ketepatan_waktu_id = 0;
			}
			if ($ketepatan_waktu_id == 0) {
				$data['ketepatan_waktu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_ketepatan_waktu_detail', ['bulan' => $post['month'], 'ketepatan_waktu_id' => $ketepatan_waktu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['ketepatan_waktu'] = $detail['hasil'];
				} else {
					$data['ketepatan_waktu'] = 0;
				}
			}

			$customer_header = $this->sql->get('tbl_customer', ['tahun' => $post['year'], "customer_type_id IN(SELECT customer_type_id FROM tbl_customer_type WHERE layanan_id = '" . $layanan['layanan_id'] . "')" => NULL]);
			if ($customer_header->num_rows() > 0) {
				$customer_header = $customer_header->result_array();
				foreach ($customer_header as $row) {
					$customer_id = $row['customer_id'];
					$detail = $this->sql->get('tbl_customer_detail', ['bulan' => $post['month'], 'customer_id' => $customer_id]);
					if ($detail->num_rows() > 0) {
						$detail = $detail->row_array();

						if ($row['customer_type_id'] == 9) {
							$data['customer_pelatihan'] = $detail['hasil'];
						}
					} else {
						if ($row['customer_type_id'] == 9) {
							$data['customer_pelatihan'] = 0;
						}
					}
				}

				if (!isset($data['customer_pelatihan'])) {
					$data['customer_pelatihan'] = 0;
				}
			} else {
				$data['customer_pelatihan'] = 0;
			}
			$this->load->view('dashboard/detail_pelatihan', $data);
		} else {
			echo "error";
		}
	}

	public function detail_rbpi()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$data['kategori'] = $post['kategori'];
			$layanan = $this->sql->get('tbl_layanan', ['slug' => $post['kategori']])->row_array();

			$sample_header = $this->sql->get('tbl_sample', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($sample_header->num_rows() > 0) {
				$sample_header = $sample_header->row_array();
				$sample_id = $sample_header['sample_id'];
			} else {
				$sample_id = 0;
			}
			if ($sample_id == 0) {
				$data['sample'] = 0;
			} else {
				$detail = $this->sql->get('tbl_sample_detail', ['bulan' => $post['month'], 'sample_id' => $sample_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['sample'] = $detail['hasil'];
				} else {
					$data['sample'] = 0;
				}
			}

			$sasaran_mutu_header = $this->sql->get('tbl_sasaran_mutu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($sasaran_mutu_header->num_rows() > 0) {
				$sasaran_mutu_header = $sasaran_mutu_header->row_array();
				$sasaran_mutu_id = $sasaran_mutu_header['sasaran_mutu_id'];
			} else {
				$sasaran_mutu_id = 0;
			}
			if ($sasaran_mutu_id == 0) {
				$data['sasaran_mutu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_sasaran_mutu_detail', ['bulan' => $post['month'], 'sasaran_mutu_id' => $sasaran_mutu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['sasaran_mutu'] = $detail['hasil'];
				} else {
					$data['sasaran_mutu'] = 0;
				}
			}

			$ketepatan_waktu_header = $this->sql->get('tbl_ketepatan_waktu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($ketepatan_waktu_header->num_rows() > 0) {
				$ketepatan_waktu_header = $ketepatan_waktu_header->row_array();
				$ketepatan_waktu_id = $ketepatan_waktu_header['ketepatan_waktu_id'];
			} else {
				$ketepatan_waktu_id = 0;
			}
			if ($ketepatan_waktu_id == 0) {
				$data['ketepatan_waktu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_ketepatan_waktu_detail', ['bulan' => $post['month'], 'ketepatan_waktu_id' => $ketepatan_waktu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['ketepatan_waktu'] = $detail['hasil'];
				} else {
					$data['ketepatan_waktu'] = 0;
				}
			}

			$customer_header = $this->sql->get('tbl_customer', ['tahun' => $post['year'], "customer_type_id IN(SELECT customer_type_id FROM tbl_customer_type WHERE layanan_id = '" . $layanan['layanan_id'] . "')" => NULL]);
			if ($customer_header->num_rows() > 0) {
				$customer_header = $customer_header->result_array();
				foreach ($customer_header as $row) {
					$customer_id = $row['customer_id'];
					$detail = $this->sql->get('tbl_customer_detail', ['bulan' => $post['month'], 'customer_id' => $customer_id]);
					if ($detail->num_rows() > 0) {
						$detail = $detail->row_array();

						if ($row['customer_type_id'] == 10) {
							$data['customer_rbpi'] = $detail['hasil'];
						}
					} else {
						if ($row['customer_type_id'] == 10) {
							$data['customer_rbpi'] = 0;
						}
					}
				}

				if (!isset($data['customer_rbpi'])) {
					$data['customer_rbpi'] = 0;
				}
			} else {
				$data['customer_rbpi'] = 0;
			}
			$this->load->view('dashboard/detail_rbpi', $data);
		} else {
			echo "error";
		}
	}

	public function detail_teknologi_proses()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$data['kategori'] = $post['kategori'];
			$layanan = $this->sql->get('tbl_layanan', ['slug' => $post['kategori']])->row_array();

			$sample_header = $this->sql->get('tbl_sample', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($sample_header->num_rows() > 0) {
				$sample_header = $sample_header->row_array();
				$sample_id = $sample_header['sample_id'];
			} else {
				$sample_id = 0;
			}
			if ($sample_id == 0) {
				$data['sample'] = 0;
			} else {
				$detail = $this->sql->get('tbl_sample_detail', ['bulan' => $post['month'], 'sample_id' => $sample_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['sample'] = $detail['hasil'];
				} else {
					$data['sample'] = 0;
				}
			}

			$sasaran_mutu_header = $this->sql->get('tbl_sasaran_mutu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($sasaran_mutu_header->num_rows() > 0) {
				$sasaran_mutu_header = $sasaran_mutu_header->row_array();
				$sasaran_mutu_id = $sasaran_mutu_header['sasaran_mutu_id'];
			} else {
				$sasaran_mutu_id = 0;
			}
			if ($sasaran_mutu_id == 0) {
				$data['sasaran_mutu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_sasaran_mutu_detail', ['bulan' => $post['month'], 'sasaran_mutu_id' => $sasaran_mutu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['sasaran_mutu'] = $detail['hasil'];
				} else {
					$data['sasaran_mutu'] = 0;
				}
			}

			$ketepatan_waktu_header = $this->sql->get('tbl_ketepatan_waktu', ['tahun' => $post['year'], 'layanan_id' => $layanan['layanan_id']]);
			if ($ketepatan_waktu_header->num_rows() > 0) {
				$ketepatan_waktu_header = $ketepatan_waktu_header->row_array();
				$ketepatan_waktu_id = $ketepatan_waktu_header['ketepatan_waktu_id'];
			} else {
				$ketepatan_waktu_id = 0;
			}
			if ($ketepatan_waktu_id == 0) {
				$data['ketepatan_waktu'] = 0;
			} else {
				$detail = $this->sql->get('tbl_ketepatan_waktu_detail', ['bulan' => $post['month'], 'ketepatan_waktu_id' => $ketepatan_waktu_id]);
				if ($detail->num_rows() > 0) {
					$detail = $detail->row_array();
					$data['ketepatan_waktu'] = $detail['hasil'];
				} else {
					$data['ketepatan_waktu'] = 0;
				}
			}

			$customer_header = $this->sql->get('tbl_customer', ['tahun' => $post['year'], "customer_type_id IN(SELECT customer_type_id FROM tbl_customer_type WHERE layanan_id = '" . $layanan['layanan_id'] . "')" => NULL]);
			if ($customer_header->num_rows() > 0) {
				$customer_header = $customer_header->result_array();
				foreach ($customer_header as $row) {
					$customer_id = $row['customer_id'];
					$detail = $this->sql->get('tbl_customer_detail', ['bulan' => $post['month'], 'customer_id' => $customer_id]);
					if ($detail->num_rows() > 0) {
						$detail = $detail->row_array();

						if ($row['customer_type_id'] == 11) {
							$data['customer_teknologi_proses'] = $detail['hasil'];
						}
					} else {
						if ($row['customer_type_id'] == 11) {
							$data['customer_teknologi_proses'] = 0;
						}
					}
				}

				if (!isset($data['customer_teknologi_proses'])) {
					$data['customer_teknologi_proses'] = 0;
				}
			} else {
				$data['customer_teknologi_proses'] = 0;
			}
			$this->load->view('dashboard/detail_teknologi_proses', $data);
		} else {
			echo "error";
		}
	}
}
