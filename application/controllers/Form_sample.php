<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Form_sample extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(['sql', 'm_custom']);
	}

	public function index()
	{
		$year = date("Y");
		if ($this->input->get()) {
			$year = $this->input->get('year');
		}
		$data['year'] = $year;
		$where = ['tahun' => $year];
        
        $sample = $this->sql->get('tbl_sample', $where)->result_array();
        $data_sample = [];
        foreach($sample as $row){
            $data_sample[$row['layanan_id']] = $row['target'];
        }
        $data['sample'] = $data_sample;

        $data['year'] = $year;
		$detail = $this->m_custom->sample($where)->result_array();
        $data['layanan'] = $this->sql->get('tbl_layanan', ["slug IN('pengujian','lingkungan','kalibrasi','pelatihan','rbpi','teknologi-proses')" => NULL])->result_array();
        

		$data_detail = [];
		foreach ($detail as $row) {
			$data_detail[$row['layanan_id']][$row['bulan']] = $row['hasil'];
		}

		$data['detail'] = $data_detail;

		$data['subview'] = "form_sample/data";
		$data['site_title'] = "Form sample";
		$this->load->view('index', $data);
	}

	function get()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

            $where = ['tahun' => $post['tahun'], 'layanan_id' => $post['layanan']];
            $check_header = $this->sql->get('tbl_sample', $where)->num_rows();
            if($check_header == 0){
                $data = [
                    'tahun' => $post['tahun'],
                    'layanan_id' => $post['layanan']
                ];
                $this->sql->create('tbl_sample', $data);
            }

            $header = $this->sql->get('tbl_sample', $where)->row_array();

            $where = [
                'sample_id' => $header['sample_id'],
                'bulan' => $post['bulan']
            ];

			$res = $this->sql->get('tbl_sample_detail', $where);
			$data['hasil'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['hasil'] = $result['hasil'];
			}
			$data['tahun'] = $post['tahun'];
			$data['bulan'] = $post['bulan'];
			$data['layanan'] = $post['layanan'];
			$this->load->view('form_sample/get', $data);
		} else {
			echo "error";
		}
	}

	function get_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
                'layanan_id' => $post['layanan']
			];

			$res = $this->sql->get("tbl_sample", $where);
			$data['target'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['target'] = $result['target'];
			}
			$data['tahun'] = $post['tahun'];
			$data['layanan'] = $post['layanan'];

			$this->load->view('form_sample/get_target', $data);
		} else {
			echo "error";
		}
	}

	function update()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

            $header = $this->sql->get('tbl_sample', ['tahun' => $post['tahun'], 'layanan_id' => $post['layanan_id']])->row_array();

			$where = [
				'sample_id' => $header['sample_id'],
				'bulan' => $post['bulan']
			];

			$check = $this->sql->get('tbl_sample_detail', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->update('tbl_sample_detail', $form_data, $where);
			} else {
				$form_data = [
					'sample_id' => $header['sample_id'],
					'bulan' => $post['bulan'],
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->create('tbl_sample_detail', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function update_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
                'layanan_id' => $post['layanan_id']
			];

			$check = $this->sql->get('tbl_sample', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'target' => $post['target']
				];
				$update = $this->sql->update('tbl_sample', $form_data, $where);
			} else {
				$form_data = [
					'tahun' => $post['tahun'],
                    'layanan_id' => $post['layanan_id'],
					'target' => $post['target']
				];
				$update = $this->sql->create('tbl_sample', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
