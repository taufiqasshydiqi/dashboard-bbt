<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Form_ketepatan_waktu extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(['sql', 'm_custom']);
	}

	public function index()
	{
		$year = date("Y");
		if ($this->input->get()) {
			$year = $this->input->get('year');
		}
		$data['year'] = $year;
		$where = ['tahun' => $year];
        
        $ketepatan_waktu = $this->sql->get('tbl_ketepatan_waktu', $where)->result_array();
        $data_ketepatan_waktu = [];
        foreach($ketepatan_waktu as $row){
            $data_ketepatan_waktu[$row['layanan_id']] = $row['target'];
        }
        $data['ketepatan_waktu'] = $data_ketepatan_waktu;

        $data['year'] = $year;
		$detail = $this->m_custom->ketepatan_waktu($where)->result_array();
        $data['layanan'] = $this->sql->get('tbl_layanan')->result_array();
        

		$data_detail = [];
		foreach ($detail as $row) {
			$data_detail[$row['layanan_id']][$row['bulan']] = $row['hasil'];
		}

		$data['detail'] = $data_detail;

		$data['subview'] = "form_ketepatan_waktu/data";
		$data['site_title'] = "Form ketepatan_waktu";
		$this->load->view('index', $data);
	}

	function get()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

            $where = ['tahun' => $post['tahun'], 'layanan_id' => $post['layanan']];
            $check_header = $this->sql->get('tbl_ketepatan_waktu', $where)->num_rows();
            if($check_header == 0){
                $data = [
                    'tahun' => $post['tahun'],
                    'layanan_id' => $post['layanan']
                ];
                $this->sql->create('tbl_ketepatan_waktu', $data);
            }

            $header = $this->sql->get('tbl_ketepatan_waktu', $where)->row_array();

            $where = [
                'ketepatan_waktu_id' => $header['ketepatan_waktu_id'],
                'bulan' => $post['bulan']
            ];

			$res = $this->sql->get('tbl_ketepatan_waktu_detail', $where);
			$data['hasil'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['hasil'] = $result['hasil'];
			}
			$data['tahun'] = $post['tahun'];
			$data['bulan'] = $post['bulan'];
			$data['layanan'] = $post['layanan'];
			$this->load->view('form_ketepatan_waktu/get', $data);
		} else {
			echo "error";
		}
	}

	function get_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
                'layanan_id' => $post['layanan']
			];

			$res = $this->sql->get("tbl_ketepatan_waktu", $where);
			$data['target'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['target'] = $result['target'];
			}
			$data['tahun'] = $post['tahun'];
			$data['layanan'] = $post['layanan'];

			$this->load->view('form_ketepatan_waktu/get_target', $data);
		} else {
			echo "error";
		}
	}

	function update()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

            $header = $this->sql->get('tbl_ketepatan_waktu', ['tahun' => $post['tahun'], 'layanan_id' => $post['layanan_id']])->row_array();

			$where = [
				'ketepatan_waktu_id' => $header['ketepatan_waktu_id'],
				'bulan' => $post['bulan']
			];

			$check = $this->sql->get('tbl_ketepatan_waktu_detail', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->update('tbl_ketepatan_waktu_detail', $form_data, $where);
			} else {
				$form_data = [
					'ketepatan_waktu_id' => $header['ketepatan_waktu_id'],
					'bulan' => $post['bulan'],
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->create('tbl_ketepatan_waktu_detail', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function update_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
                'layanan_id' => $post['layanan_id']
			];

			$check = $this->sql->get('tbl_ketepatan_waktu', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'target' => $post['target']
				];
				$update = $this->sql->update('tbl_ketepatan_waktu', $form_data, $where);
			} else {
				$form_data = [
					'tahun' => $post['tahun'],
                    'layanan_id' => $post['layanan_id'],
					'target' => $post['target']
				];
				$update = $this->sql->create('tbl_ketepatan_waktu', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
