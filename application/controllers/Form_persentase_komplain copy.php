<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Form_persentase_komplain extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['sql', 'm_custom']);
    }

    public function index()
    {
        $year = date("Y");
        if ($this->input->get()) {
            $year = $this->input->get('year');
        }
        $data['year'] = $year;
        $where = ['tahun' => $year];
        $data['year'] = $year;
        $data['persentase_komplain'] = $this->sql->get('tbl_persentase_komplain', $where)->result_array();
        $detail = $this->m_custom->persentase_komplain($where)->result_array();

        $data_detail = [];
        foreach ($detail as $row) {
            $data_detail[$row['persentase_komplain_id']][$row['bulan']] = $row['hasil'];
        }

        $data['detail'] = $data_detail;

        $data['subview'] = "form_persentase_komplain/data";
        $data['site_title'] = "Form persentase_komplain";
        $this->load->view('index', $data);
    }

    function get()
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $where = ['tahun' => $post['tahun']];
            $check_header = $this->sql->get('tbl_persentase_komplain', $where)->num_rows();
            if ($check_header == 0) {
                $data = [
                    'tahun' => $post['tahun']
                ];
                $this->sql->create('tbl_persentase_komplain', $data);
            }

            $header = $this->sql->get('tbl_persentase_komplain', $where)->row_array();

            $where = [
                'persentase_komplain_id' => $header['persentase_komplain_id'],
                'bulan' => $post['bulan']
            ];

            $res = $this->sql->get('tbl_persentase_komplain_detail', $where);
            $data['hasil'] = 0;
            if ($res->num_rows() > 0) {
                $result = $res->row_array();
                $data['hasil'] = $result['hasil'];
            }
            $data['tahun'] = $post['tahun'];
            $data['bulan'] = $post['bulan'];
            $this->load->view('form_persentase_komplain/get', $data);
        } else {
            echo "error";
        }
    }

    function get_target()
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $where = [
                'tahun' => $post['tahun']
            ];

            $res = $this->sql->get("tbl_persentase_komplain", $where);
            $data['target'] = 0;
            if ($res->num_rows() > 0) {
                $result = $res->row_array();
                $data['target'] = $result['target'];
            }
            $data['tahun'] = $post['tahun'];

            $this->load->view('form_persentase_komplain/get_target', $data);
        } else {
            echo "error";
        }
    }

    function update()
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $header = $this->sql->get('tbl_persentase_komplain', ['tahun' => $post['tahun']])->row_array();

            $where = [
                'persentase_komplain_id' => $header['persentase_komplain_id'],
                'bulan' => $post['bulan']
            ];

            $check = $this->sql->get('tbl_persentase_komplain_detail', $where);

            if ($check->num_rows() > 0) {
                $form_data = [
                    'hasil' => $post['jumlah']
                ];
                $update = $this->sql->update('tbl_persentase_komplain_detail', $form_data, $where);
            } else {
                $form_data = [
                    'persentase_komplain_id' => $header['persentase_komplain_id'],
                    'bulan' => $post['bulan'],
                    'hasil' => $post['jumlah']
                ];
                $update = $this->sql->create('tbl_persentase_komplain_detail', $form_data);
            }

            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function update_target()
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $where = [
                'tahun' => $post['tahun']
            ];

            $check = $this->sql->get('tbl_persentase_komplain', $where);

            if ($check->num_rows() > 0) {
                $form_data = [
                    'target' => $post['target']
                ];
                $update = $this->sql->update('tbl_persentase_komplain', $form_data, $where);
            } else {
                $form_data = [
                    'tahun' => $post['tahun'],
                    'target' => $post['target']
                ];
                $update = $this->sql->create('tbl_persentase_komplain', $form_data);
            }

            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
}
