<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Form_customer extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(['sql', 'm_custom']);
	}

	public function index()
	{
		$year = date("Y");
		if ($this->input->get()) {
			$year = $this->input->get('year');
		}
		$data['year'] = $year;
		$where = ['tahun' => $year];
        
        $customer = $this->sql->get('tbl_customer', $where)->result_array();
        $data_customer = [];
        foreach($customer as $row){
            $data_customer[$row['customer_type_id']] = $row['target'];
        }
        $data['customer'] = $data_customer;

        $data['year'] = $year;
		$detail = $this->m_custom->customer($where)->result_array();
        $data['layanan'] = $this->m_custom->customer_type()->result_array();
        

		$data_detail = [];
		foreach ($detail as $row) {
			$data_detail[$row['customer_type_id']][$row['bulan']] = $row['hasil'];
		}

		$data['detail'] = $data_detail;

		$data['subview'] = "form_customer/data";
		$data['site_title'] = "Form customer";
		$this->load->view('index', $data);
	}

	function get()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

            $where = ['tahun' => $post['tahun'], 'customer_type_id' => $post['layanan']];
            $check_header = $this->sql->get('tbl_customer', $where)->num_rows();
            if($check_header == 0){
                $data = [
                    'tahun' => $post['tahun'],
                    'customer_type_id' => $post['layanan']
                ];
                $this->sql->create('tbl_customer', $data);
            }

            $header = $this->sql->get('tbl_customer', $where)->row_array();

            $where = [
                'customer_id' => $header['customer_id'],
                'bulan' => $post['bulan']
            ];

			$res = $this->sql->get('tbl_customer_detail', $where);
			$data['hasil'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['hasil'] = $result['hasil'];
			}
			$data['tahun'] = $post['tahun'];
			$data['bulan'] = $post['bulan'];
			$data['layanan'] = $post['layanan'];
			$this->load->view('form_customer/get', $data);
		} else {
			echo "error";
		}
	}

	function get_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
                'customer_type_id' => $post['layanan']
			];

			$res = $this->sql->get("tbl_customer", $where);
			$data['target'] = 0;
			if ($res->num_rows() > 0) {
				$result = $res->row_array();
				$data['target'] = $result['target'];
			}
			$data['tahun'] = $post['tahun'];
			$data['layanan'] = $post['layanan'];

			$this->load->view('form_customer/get_target', $data);
		} else {
			echo "error";
		}
	}

	function update()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

            $header = $this->sql->get('tbl_customer', ['tahun' => $post['tahun'], 'customer_type_id' => $post['customer_type_id']])->row_array();

			$where = [
				'customer_id' => $header['customer_id'],
				'bulan' => $post['bulan']
			];

			$check = $this->sql->get('tbl_customer_detail', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->update('tbl_customer_detail', $form_data, $where);
			} else {
				$form_data = [
					'customer_id' => $header['customer_id'],
					'bulan' => $post['bulan'],
					'hasil' => $post['jumlah']
				];
				$update = $this->sql->create('tbl_customer_detail', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function update_target()
	{
		if ($this->input->post()) {
			$post = $this->input->post();

			$where = [
				'tahun' => $post['tahun'],
                'customer_type_id' => $post['customer_type_id']
			];

			$check = $this->sql->get('tbl_customer', $where);

			if ($check->num_rows() > 0) {
				$form_data = [
					'target' => $post['target']
				];
				$update = $this->sql->update('tbl_customer', $form_data, $where);
			} else {
				$form_data = [
					'tahun' => $post['tahun'],
                    'customer_type_id' => $post['customer_type_id'],
					'target' => $post['target']
				];
				$update = $this->sql->create('tbl_customer', $form_data);
			}

			redirect($_SERVER['HTTP_REFERER']);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
