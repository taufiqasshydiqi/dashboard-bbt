<div class="modal-header">
    <h5 class="modal-title" id="formModal1">Form Input</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form action="<?= base_url() ?>form_pnbp/update/<?=$id?>" method="POST">
    <div class="modal-body">
        <div class="form-group">
            <label>Tahun <span class="text-danger">*</span></label>
            <input type="number" class="form-control" name="tahun" placeholder="Tahun" value="<?= $tahun ?>" required readonly>
        </div>
        <div class="form-group">
            <label>Bulan <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="month" placeholder="Bulan" value="<?= month_indo($bulan) ?>" required readonly>
            <input type="hidden" name="bulan" value="<?= $bulan ?>" required>
            <input type="hidden" name="layanan" value="<?= $layanan ?>" required>
        </div>
        <div class="form-group">
            <label>Jumlah <span class="text-danger">*</span></label>
            <input type="number" name="jumlah" class="form-control" placeholder="Jumlah" value="<?= $hasil ?>" required>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
</form>