<div class="app-content page-body">
	<div class="container-fluid">

		<!--Row-->
		<div class="row">
			<div class="col-xl-12 col-md-12 col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class=" col-xl col-sm-6 d-flex mb-5 mb-xl-0">
								<div class="feature ">
									<i class="si si-briefcase primary feature-icon bg-primary"></i>
								</div>
								<div class="ml-3">
									<small class=" mb-0">JUMLAH STAFF PENGUJIAN</small><br>
									<h3 class="font-weight-semibold mb-0">3.5</h3>
									<small class="mb-0 text-muted"><span class="text-success font-weight-semibold"><i class="fa fa-caret-up  mr-1"></i> 0.67%</span> Last year</small>
								</div>
							</div>
							<div class=" col-xl col-sm-6 d-flex mb-5 mb-xl-0">
								<div class="feature">
									<i class="si si-layers danger feature-icon bg-danger"></i>
								</div>
								<div class=" d-flex flex-column  ml-3"> <small class=" mb-0">JUMLAH STAFF KALIBRASI</small>
									<h3 class="font-weight-semibold mb-0">2,536</h3>
									<small class="mb-0 text-muted"><span class="text-success font-weight-semibold"><i class="fa fa-caret-up  mr-1"></i> 0.25%</span> Last year</small>
								</div>
							</div>
							<div class=" col-xl col-sm-6 d-flex  mb-5 mb-sm-0">
								<div class="feature">
									<i class="si si-note secondary feature-icon bg-secondary"></i>
								</div>
								<div class=" d-flex flex-column ml-3"> <small class=" mb-0">JUMLAH STAFF LINGKUNGAN</small>
									<h3 class="font-weight-semibold mb-0">1</h3>
									<small class="mb-0 text-muted"><span class="text-success font-weight-semibold"><i class="fa fa-caret-up  mr-1"></i> 0.67%</span> Last year</small>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End row-->
		<!--Row-->
		<div class="row">
			<div class="col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<div class="card-title">Indikator Output Kegiatan Tahun Anggaran 2019 Balai Besar Tekstil</div>
						<div class="card-options ">
							<a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
							<a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table  table-vcenter border mb-0 text-nowrap">
								<thead class="">
									<tr>
										<th>Nama</th>
										<th>Tanggal Terima Order</th>
										<th>Tanggal Kirim Order</th>
										<th>Status Order</th>
										<th>Lama Waktu</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-sm font-weight-600">A</td>
										<td>12 Agustus 2021</td>
										<td>14 Agustus 2021</td>
										<td>Selesai</td>
										<td>2 Hari</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
									</tr>
									<tr>
										<td class="text-sm font-weight-600">A</td>
										<td>12 Agustus 2021</td>
										<td>14 Agustus 2021</td>
										<td>Selesai</td>
										<td>2 Hari</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
									</tr>
									<tr>
										<td class="text-sm font-weight-600">A</td>
										<td>12 Agustus 2021</td>
										<td>14 Agustus 2021</td>
										<td>Selesai</td>
										<td>2 Hari</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
									</tr>
									<tr>
										<td class="text-sm font-weight-600">A</td>
										<td>12 Agustus 2021</td>
										<td>14 Agustus 2021</td>
										<td>Selesai</td>
										<td>2 Hari</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
									</tr>
									<tr>
										<td class="text-sm font-weight-600">A</td>
										<td>12 Agustus 2021</td>
										<td>14 Agustus 2021</td>
										<td>Selesai</td>
										<td>2 Hari</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
										<td>0</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End row-->
	</div>
</div><!-- end app-content-->
</div>