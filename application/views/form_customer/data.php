<div class="app-content page-body">
	<div class="container">

		<!--Page header-->
		<div class="page-header">
			<div class="page-leftheader">
				<h4 class="page-title">Form Customer</h4>
				<ol class="breadcrumb pl-0">
					<li class="breadcrumb-item"><a href="#">Form</a></li>
					<li class="breadcrumb-item active" aria-current="page">Form Customer</li>
				</ol>
			</div>
			<div class="page-rightheader">
				<div class="ml-3 ml-auto d-flex">
					<div class="">
						<div class="pr-4 mt-2 d-xl-block">
							<p class="text-muted mb-1">Category</p>
							<h6 class="font-weight-semibold mb-0">customer</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End Page header-->

		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="card">
					<div class="card-header">
						<div class="card-title">Form Customer</div>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-4">
								<div class="form-group">
									<label>Tahun</label>
									<select name="year" class="form-control custom-select" required>
										<?php for ($i = date('Y'); $i >= 2002; $i--) { ?>
											<option value="<?= $i ?>" <?= ($i == $year) ? 'selected' : ''; ?>><?= $i ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Layanan</th>
										<th>Type</th>
										<th>Target</th>
										<?php for ($i = 1; $i <= 12; $i++) { ?>
											<th><?= month_indo($i) ?></th>
										<?php } ?>
									</tr>
								</thead>
								<tbody>
									<?php
									if (count($customer) > 0) {
										foreach ($layanan as $result) {
									?>
											<tr>
												<td><?= $result['nama_layanan'] ?></td>
												<td><?= $result['type'] ?></td>
												<td>
													<?php if (!isset($customer[$result['customer_type_id']])) { ?>
														<button type="button" data-toggle="modal" data-target="#targetModal" data-tahun="<?= $year ?>" data-layanan="<?= $result['customer_type_id'] ?>" class="btn btn-danger btn-sm"><i class="fa fa-edit"></i>Fill</button>
													<?php } else {
													?>
														<button type="button" data-toggle="modal" data-target="#targetModal" data-tahun="<?= $year ?>" data-layanan="<?= $result['customer_type_id'] ?>" class="btn btn-success btn-sm">
															<?= number_format($customer[$result['customer_type_id']]) ?></button>
													<?php
													} ?>
												</td>
												<?php for ($i = 1; $i <= 12; $i++) { ?>
													<td>
														<?php if (isset($detail[$result['customer_type_id']][$i])) { ?>
															<button type="button" data-toggle="modal" data-target="#formModal" data-tahun="<?= $year ?>" data-bulan="<?= $i ?>" data-layanan="<?= $result['customer_type_id'] ?>" class="btn btn-success btn-sm"><?= number_format($detail[$result['customer_type_id']][$i]) ?></button>
														<?php } else { ?>
															<button type="button" data-toggle="modal" data-target="#formModal" data-tahun="<?= $year ?>" data-bulan="<?= $i ?>" data-layanan="<?= $result['customer_type_id'] ?>" class="btn btn-danger btn-sm"><i class="fa fa-edit"></i>Fill</button>
														<?php } ?>
													</td>
												<?php } ?>
											</tr>
										<?php
										}
									} else {
										foreach ($layanan as $result) {
										?>
											<tr>
												<td><?= $result['nama_layanan'] ?></td>
												<td><?= $result['type'] ?></td>
												<td><button type="button" data-toggle="modal" data-target="#targetModal" data-tahun="<?= $year ?>" data-layanan="<?= $result['customer_type_id'] ?>" class="btn btn-danger btn-sm"><i class="fa fa-edit"></i>Fill</button></td>
												<?php for ($i = 1; $i <= 12; $i++) { ?>
													<td>
														<button type="button" data-toggle="modal" data-target="#formModal" data-tahun="<?= $year ?>" data-bulan="<?= $i ?>" data-layanan="<?= $result['customer_type_id'] ?>" class="btn btn-danger btn-sm"><i class="fa fa-edit"></i>Fill</button>
													</td>
												<?php } ?>
											</tr>
									<?php }
									} ?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- table-wrapper -->
				</div>
				<!-- section-wrapper -->
			</div>
		</div>

	</div>
</div><!-- end app-content-->
</div>
<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
	<div class="modal-dialog modal-full" role="document">
		<div class="modal-content">
			<div id="modal-data"></div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="targetModal" tabindex="-1" role="dialog" aria-labelledby="targetModal" aria-hidden="true">
	<div class="modal-dialog modal-full" role="document">
		<div class="modal-content">
			<div id="target-data"></div>
		</div>
	</div>
</div>
<script>
	$("#formModal").on('show.bs.modal', function(e) {
		var tahun = $(e.relatedTarget).data('tahun');
		var bulan = $(e.relatedTarget).data('bulan');
		var layanan = $(e.relatedTarget).data('layanan');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>form_customer/get",
			data: {
				tahun: tahun,
				bulan: bulan,
				layanan: layanan
			}
		}).done(function(response) {
			if (response != 'error') {
				$("#modal-data").html(response);
			} else {
				alert('Error');
			}
		});
	});

	$("#targetModal").on('show.bs.modal', function(e) {
		var tahun = $(e.relatedTarget).data('tahun');
		var layanan = $(e.relatedTarget).data('layanan');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>form_customer/get_target",
			data: {
				tahun: tahun,
				layanan: layanan
			}
		}).done(function(response) {
			if (response != 'error') {
				$("#target-data").html(response);
			} else {
				alert('Error');
			}
		});
	});

	$(function() {
		// bind change event to select
		$('[name=year]').on('change', function() {
			var year = $(this).val(); // get selected value
			if (year) { // require a year
				window.location = "<?= base_url() ?>form_customer/index?year=" + year; // redirect
			}
			return false;
		});
	});
</script>