<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>

    <!-- Meta data -->
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta content="Dashboard BBT" name="description">
    <meta content="Balai Besar Tekstil" name="author">
    <meta name="keywords" content="bbt balai besar tekstil dashboard" />

    <!-- Title -->
    <title><?= $site_title ?> – Dashboard BBT</title>

    <!-- Form Login  -->
    <link href="<?= base_url() ?>assets/css/styles.css" rel="stylesheet" />

    <!--Favicon -->
    <link rel="icon" href="<?= base_url() ?>assets/images/logo.png" type="image/x-icon" />

    <!-- Style css -->
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet" />

    <!--Horizontal css -->
    <link id="effect" href="<?= base_url() ?>assets/plugins/horizontal-menu/dropdown-effects/fade-up.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/plugins/horizontal-menu/horizontal.css" rel="stylesheet" />

    <!-- P-scroll bar css-->
    <link href="<?= base_url() ?>assets/plugins/p-scroll/perfect-scrollbar.css" rel="stylesheet" />

    <!-- Data table css -->
    <link href="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />

    <!---Icons css-->
    <link href="<?= base_url() ?>assets/plugins/iconfonts/icons.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/plugins/iconfonts/font-awesome/font-awesome.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/plugins/iconfonts/plugin.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet" />

    <!-- Skin css-->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="<?= base_url() ?>assets/skins/hor-skin/horizontal-dark.css" />


    <!-- Jquery js-->
    <script src="<?= base_url() ?>assets/js/vendors/jquery-3.4.0.min.js"></script>

    <!-- Bootstrap4 js-->
    <script src="<?= base_url() ?>assets/plugins/bootstrap/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <!--Othercharts js-->
    <script src="<?= base_url() ?>assets/plugins/othercharts/jquery.sparkline.min.js"></script>

    <!-- Circle-progress js-->
    <script src="<?= base_url() ?>assets/js/vendors/circle-progress.min.js"></script>

    <!-- Jquery-rating js-->
    <script src="<?= base_url() ?>assets/plugins/rating/jquery.rating-stars.js"></script>

    <!--Horizontal js-->
    <script src="<?= base_url() ?>assets/plugins/horizontal-menu/horizontal.js"></script>

    <!-- P-scroll js-->
    <script src="<?= base_url() ?>assets/plugins/p-scroll/perfect-scrollbar.js"></script>

    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
</head>

<body class="app">

    <!---Global-loader-->
    <div id="global-loader">
        <img src="<?= base_url() ?>assets/images/svgs/loader.svg" alt="loader">
    </div>

    <div class="page-login">
        <div class="page-main">
            <div class="header top-header">
                <div class="container">
                    <div class="d-flex justify-content-center">
                        <a class="header-brand" href="index.html">
                            <img src="<?= base_url() ?>assets/images/logo.png" class="header-brand-img desktop-lgo" alt="BBT logo">
                            <img src="<?= base_url() ?>assets/images/logo.png" class="header-brand-img mobile-logo" alt="BBT logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Login -->
        <div class="row justify-content-center pt-9 pb-8">
            <div class="col-lg-5">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header">
                        <h3 class="text-center font-weight-light my-4">Login</h3>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-floating mb-3">
                                <input class="form-control" id="inputEmail" type="email" placeholder="name@example.com" />
                                <label for="inputEmail">Email address</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control" id="inputPassword" type="password" placeholder="Password" />
                                <label for="inputPassword">Password</label>
                            </div>
                            <div class="d-flex align-items-center justify-content-between mt-4 mb-0">
                                <a class="btn btn-primary" href="index.html">Login</a>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer text-center py-3">
                        <div class="small"><a href="register.html">Need an account? Sign up!</a></div>
                    </div>
                </div>
            </div>
        </div>




        <!--Footer-->
        <footer class="footer">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
                        Copyright © 2021 <a href="#">Balai Besar Tekstil</a>.
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer-->

    </div>

    <!-- Back to top -->
    <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    <!-- Index js-->
    <script src="<?= base_url() ?>assets/js/index3.js"></script>

    <!-- Custom js-->
    <script src="<?= base_url() ?>assets/js/custom-dark.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="assets/js/scripts.js"></script>

</body>

</html>