<!--Footer-->
<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
							Copyright © 2021 <a href="#">Balai Besar Tekstil</a>.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->

		</div>

		<!-- Back to top -->
		<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

		<!-- Index js-->
		<script src="<?=base_url()?>assets/js/index3.js"></script>

		<!-- Custom js-->
		<script src="<?=base_url()?>assets/js/custom-dark.js"></script>

	</body>
</html>