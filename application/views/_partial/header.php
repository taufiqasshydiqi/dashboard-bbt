<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>

    <!-- Meta data -->
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta content="Dashboard BBT" name="description">
    <meta content="Balai Besar Tekstil" name="author">
    <meta name="keywords" content="bbt balai besar tekstil dashboard" />

    <!-- Title -->
    <title><?= $site_title ?> – Dashboard BBT</title>

    <!--Favicon -->
    <link rel="icon" href="<?= base_url() ?>assets/images/logo.png" type="image/x-icon" />

    <!-- Style css -->
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet" />

    <!--Horizontal css -->
    <link id="effect" href="<?= base_url() ?>assets/plugins/horizontal-menu/dropdown-effects/fade-up.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/plugins/horizontal-menu/horizontal.css" rel="stylesheet" />

    <!-- P-scroll bar css-->
    <link href="<?= base_url() ?>assets/plugins/p-scroll/perfect-scrollbar.css" rel="stylesheet" />

    <!-- Data table css -->
    <link href="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />

    <!---Icons css-->
    <link href="<?= base_url() ?>assets/plugins/iconfonts/icons.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/plugins/iconfonts/font-awesome/font-awesome.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/plugins/iconfonts/plugin.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet" />

    <!-- Skin css-->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="<?= base_url() ?>assets/skins/hor-skin/horizontal-dark.css" />


    <!-- Jquery js-->
    <script src="<?= base_url() ?>assets/js/vendors/jquery-3.4.0.min.js"></script>

    <!-- Bootstrap4 js-->
    <script src="<?= base_url() ?>assets/plugins/bootstrap/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <!--Othercharts js-->
    <script src="<?= base_url() ?>assets/plugins/othercharts/jquery.sparkline.min.js"></script>

    <!-- Circle-progress js-->
    <script src="<?= base_url() ?>assets/js/vendors/circle-progress.min.js"></script>

    <!-- Jquery-rating js-->
    <script src="<?= base_url() ?>assets/plugins/rating/jquery.rating-stars.js"></script>

    <!--Horizontal js-->
    <script src="<?= base_url() ?>assets/plugins/horizontal-menu/horizontal.js"></script>

    <!-- P-scroll js-->
    <script src="<?= base_url() ?>assets/plugins/p-scroll/perfect-scrollbar.js"></script>

    <!-- ECharts js -->
    <script src="<?= base_url() ?>assets/plugins/echarts/echarts.js"></script>

    <!-- Peitychart js-->
    <script src="<?= base_url() ?>assets/plugins/peitychart/jquery.peity.min.js"></script>
    <script src="<?= base_url() ?>assets/plugins/peitychart/peitychart.init.js"></script>

    <!-- Apexchart js-->
    <script src="<?= base_url() ?>assets/js/apexcharts.js"></script>

    <!-- Chartjs js -->
    <script src="<?= base_url() ?>assets/plugins/chart/chart.bundle.js"></script>
    <script src="<?= base_url() ?>assets/plugins/chart/utils.js"></script>

    <!--Morris Charts js-->
    <script src="<?= base_url() ?>assets/plugins/morris/raphael-min.js"></script>
    <script src="<?= base_url() ?>assets/plugins/morris/morris.js"></script>

    <!-- Data tables js-->
    <script src="<?= base_url() ?>assets/plugins/datatable/jquery.dataTables.min.js"></script>
    <script src="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url() ?>assets/js/datatables.js"></script>
</head>

<body class="app">

    <!---Global-loader-->
    <div id="global-loader">
        <img src="<?= base_url() ?>assets/images/svgs/loader.svg" alt="loader">
    </div>

    <div class="page">
        <div class="page-main">
            <div class="header top-header">
                <div class="container">
                    <div class="d-flex">
                        <a id="horizontal-navtoggle" class="animated-arrow hor-toggle"><span></span></a><!-- sidebar-toggle-->
                        <a class="header-brand" href="index.html">
                            <img src="<?= base_url() ?>assets/images/logo.png" class="header-brand-img desktop-lgo" alt="BBT logo">
                            <img src="<?= base_url() ?>assets/images/logo.png" class="header-brand-img mobile-logo" alt="BBT logo">
                        </a>
                        <div class="mt-1">
                            <form class="form-inline">
                                <div class="search-element">
                                    <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" tabindex="1">
                                    <button class="btn btn-primary-color" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div><!-- SEARCH -->
                        <div class="d-flex order-lg-2 ml-auto">
                            <a href="#" data-toggle="search" class="nav-link nav-link-lg d-md-none navsearch"><i class="fa fa-search"></i></a>
                            <div class="dropdown   header-fullscreen">
                                <a class="nav-link icon full-screen-link" id="fullscreen-button">
                                    <i class="mdi mdi-arrow-collapse"></i>
                                </a>
                            </div>
                            <div class="dropdown header-notify">
                                <a class="nav-link icon" data-toggle="dropdown">
                                    <i class="mdi mdi-bell-outline"></i>
                                    <span class=" bg-success pulse-success "></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow pt-0" style="z-index: 999;">
                                    <div class="dropdown-header border-bottom p-4 pt-0 mb-3 w-270">
                                        <div class="d-flex">
                                            <h5 class="dropdown-title float-left mb-1 font-weight-semibold text-drak">Notifications</h5>
                                            <a href="#" class="fe fe-align-justify text-right float-right ml-auto text-muted"></a>
                                        </div>
                                    </div>
                                    <a href="#" class="dropdown-item d-flex pb-2 pt-2">
                                        <div class="card box-shadow-0 mb-0">
                                            <div class="card-body p-3">
                                                <div class="notifyimg bg-gradient-primary border-radius-4">
                                                    <i class="mdi mdi-email-outline"></i>
                                                </div>
                                                <div>
                                                    <div>Message Sent.</div>
                                                    <div class="small text-muted">3 hours ago</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="dropdown-item d-flex  pb-2">
                                        <div class="card box-shadow-0 mb-0 ">
                                            <div class="card-body p-3">
                                                <div class="notifyimg bg-gradient-danger border-radius-4 bg-danger">
                                                    <i class="fe fe-shopping-cart"></i>
                                                </div>
                                                <div>
                                                    <div> Order Placed</div>
                                                    <div class="small text-muted">5 hour ago</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="dropdown-item d-flex pb-2">
                                        <div class="card box-shadow-0 mb-0">
                                            <div class="card-body p-3">
                                                <div class="notifyimg bg-gradient-success  border-radius-4 bg-success mr-2">
                                                    <i class="fe fe-airplay"></i>
                                                </div>
                                                <div>
                                                    <div>Your Admin launched</div>
                                                    <div class="small text-muted">1 daya ago</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <div class=" text-center p-2 border-top mt-3">
                                        <a href="#" class="">View All Notifications</a>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown ">
                                <a class="nav-link pr-0 leading-none" href="#" data-toggle="dropdown" aria-expanded="false">
                                    <div class="profile-details mt-2">
                                        <span class="mr-3 font-weight-semibold">Rudi</span>
                                        <small class="text-muted mr-3">Super Admin</small>
                                    </div>
                                    <img class="avatar avatar-md brround" src="<?= base_url() ?>assets/images/users/1.jpg" alt="image">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow w-250">
                                    <div class="user-profile border-bottom p-3">
                                        <div class="user-image"><img class="user-images" src="<?= base_url() ?>assets/images/users/1.jpg" alt="image"></div>
                                        <div class="user-details">
                                            <h4>Anna Sthesia</h4>
                                            <p class="mb-1 fs-13 text-muted">AnnaSthesia@gmail.com</p>
                                        </div>
                                    </div>
                                    <a href="#" class="dropdown-item pt-3 pb-3"><i class="dropdown-icon mdi mdi-account-outline text-primary "></i> My Profile</a>
                                    <a href="#" class="dropdown-item pt-3 pb-3"><i class="dropdown-icon mdi  mdi-message-outline text-primary"></i> Messages <span class="badge badge-pill badge-success">41</span></a>
                                    <a href="#" class="dropdown-item pt-3 pb-3"><i class="dropdown-icon  mdi mdi-settings text-primary"></i> Setting</a>
                                    <a href="#" class="dropdown-item pt-3 pb-3"><i class="dropdown-icon mdi mdi-help-circle-outline text-primary"></i> FAQ</a>
                                    <a href="#" class="dropdown-item pt-3 pb-3"><i class="dropdown-icon  mdi  mdi-logout-variant text-primary"></i>Sign Out</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Horizontal-menu -->
            <div class="horizontal-main hor-menu clearfix">
                <div class="horizontal-mainwrapper container clearfix">
                    <nav class="horizontalMenu clearfix">
                        <ul class="horizontalMenu-list">
                            <li aria-haspopup="true"><a href="#" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i> Dashboard <i class="fa fa-angle-down horizontal-icon"></i></a>
                                <ul class="sub-menu">
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>dashboard">Dashboard Jasa Teknis Industri</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>dashboard/dashboard_kelembagaan">Dashboard Kelembagaan Balai Besar</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>dashboard/dashboard_sdm">Dashboard SDM</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>dashboard/dashboard_pelanggan">Dashboard Informasi Pelanggan</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>dashboard/dashboard_mesin">Dashboard Mesin</a></li>
                                </ul>
                            </li>
                            <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="typcn typcn-document-text hor-icon"></i> Form Isian <i class="fa fa-angle-down horizontal-icon"></i></a>
                                <ul class="sub-menu">
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>form_pnbp">PNBP</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>form_anggaran_rm">Anggaran RM</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>form_ikm">Indeks Kepuasan Masyarakat (IKM)</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>form_ipk">Indeks Persepsi Korupsi (IPK)</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>form_pemenuhan_standard">Pemenuhan Standard Waktu Layanan</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>form_persentase_komplain">Persentase Komplain</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>form_sample">Sample / Alat / Pelatihan / Teknologi Inkubasi</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>form_wo">Work Order</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>form_ketepatan_waktu">Ketepatan Waktu</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>form_customer">Customer</a></li>
                                    <li aria-haspopup="true"><a href="<?= base_url() ?>form_sasaran_mutu">Capaian Sasaran Mutu</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!--Nav end -->
                </div>
            </div>
            <!-- Horizontal-menu end -->