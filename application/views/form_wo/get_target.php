<div class="modal-header">
    <h5 class="modal-title" id="formModal1">Form Input Target</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form action="<?= base_url() ?>form_wo/update_target" method="POST">
    <div class="modal-body">
        <div class="form-group">
            <label>Tahun <span class="text-danger">*</span></label>
            <input type="number" class="form-control" name="tahun" placeholder="Tahun" value="<?= $tahun ?>" required readonly>
        </div>
        <div class="form-group">
            <label>Target <span class="text-danger">*</span></label>
            <input type="number" name="target" class="form-control" placeholder="Target" pattern="[0-9]+([\.,][0-9]+)?" step="0.01" value="<?= $target ?>" required>
            <input type="hidden" name="layanan_id" value="<?= $layanan ?>">
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
</form>