<div class="modal-header">
    <h5 class="modal-title" id="pnbpModal1">PNBP </h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<div class="modal-body">
    <div class="page-header mt-0">
        <div class="page-leftheader">
            <h4 class="page-title">PNBP Pelatihan</h4>
            <ol class="breadcrumb pl-0">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item">Dashboard Jasa Teknis Industri</li>
                <li class="breadcrumb-item active" aria-current="page">PNBP Pelatihan</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex clearfix">
                        <div class="text-left ">
                            <p class="card-text mb-1">Jumlah Pelatihan yang Dilaksanakan</p>
                            <h2 class="mb-1 font-weight-semibold mainvalue"><?= number_format($sample) ?></h2>
                        </div>
                        <div class="ml-auto">
                            <span class="bg-primary-transparent icon-service text-primary ">
                                <i class="fe fe-zap"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex clearfix">
                        <div class="text-left ">
                            <p class="card-text mb-1">Jumlah Peserta Pelatihan</p>
                            <h2 class="mb-1 font-weight-semibold mainvalue"><?= number_format($customer_pelatihan) ?></h2>
                        </div>
                        <div class="ml-auto">
                            <span class="bg-danger-transparent icon-service text-primary ">
                                <i class="fe fe-list text-danger"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex clearfix">
                        <div class="text-left ">
                            <p class="card-text mb-1">Capaian Sasaran Mutu</p>
                            <h2 class="mb-1 font-weight-semibold mainvalue"><?= number_format($sasaran_mutu) ?>%</h2>
                        </div>
                        <div class="ml-auto">
                            <span class="bg-secondary-transparent icon-service text-primary ">
                                <i class="fe fe-percent text-secondary"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex clearfix">
                        <div class="text-left ">
                            <p class="card-text mb-1">Ketepatan Waktu Layanan</p>
                            <h2 class="mb-1 font-weight-semibold mainvalue"><?= number_format($ketepatan_waktu) ?>%</h2>
                        </div>
                        <div class="ml-auto">
                            <span class="bg-success-transparent icon-service text-primary ">
                                <i class="fe fe-percent text-success"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>