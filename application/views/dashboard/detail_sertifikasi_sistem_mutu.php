<div class="modal-header">
    <h5 class="modal-title" id="pnbpModal1">PNBP </h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<div class="modal-body">
    <div class="page-header mt-0">
        <div class="page-leftheader">
            <h4 class="page-title">PNBP Sertifikasi Sistem Mutu</h4>
            <ol class="breadcrumb pl-0">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item">Dashboard Jasa Teknis Industri</li>
                <li class="breadcrumb-item active" aria-current="page">PNBP Sertifikasi Sistem Mutu</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex clearfix">
                        <div class="text-left ">
                            <p class="card-text mb-1">Ketepatan Waktu Layanan</p>
                            <h2 class="mb-1 font-weight-semibold mainvalue"><?= number_format($ketepatan_waktu) ?>%</h2>
                        </div>
                        <div class="ml-auto">
                            <span class="bg-success-transparent icon-service text-primary ">
                                <i class="fe fe-percent text-success"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Total Customer</h3>
                    <small class="text-muted">Total Customer</small>
                    <?php
                    $total_customer = ($customer_sistem_mutu);
                    ?>
                    <h3 class="font-weight-semibold"><?= number_format($total_customer) ?></h3>
                    <div class="progress grouped h-3">
                        <div class="progress-bar w-100 bg-primary " role="progressbar"></div>
                    </div>
                    <div class="row mt-3 pt-3">
                        <div class="col border-right">
                            <p class=" number-font1 mb-0"><span class="dot-label bg-primary"></span>Customer Sertifikasi Sistem Mutu</p>
                            <h5 class="mt-2 font-weight-semibold mb-0"><?= number_format($customer_sistem_mutu) ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>