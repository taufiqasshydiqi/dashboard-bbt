<div class="app-content page-body">
	<div class="container">

		<!--Page header-->
		<div class="page-header">
			<div class="page-leftheader">
				<h4 class="page-title">Dashboard SDM</h4>
				<ol class="breadcrumb pl-0">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Dashboard SDM</li>
				</ol>
			</div>
			<div class="display-6 font-family-sans-serif text-uppercase font-weight-bold">
				simintal
			</div>
			<div class="page-rightheader">
				<div class="ml-3 ml-auto d-flex">
					<div class="">
						<div class="border-right pr-4 mt-2 d-xl-block">
							<p class="text-muted mb-1">Category</p>
							<h6 class="font-weight-semibold mb-0">SDM</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End Page header-->

		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
				<div class="table-responsive">
					<table class="table table-striped" id="example">
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Analis</th>
								<th>Pemesanan</th>
								<th>Parameter</th>
								<th>Status</th>
								<th>Tanggal Perkiraan Selesai</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1;
							foreach ($history as $get) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $get['pengguna'] ?></td>
									<td><?= $get['kd_pemesanan'] ?></td>
									<td><?= $get['nama_uji'] ?></td>
									<td><?php
										if ($get['id_anggota'] == null) {
											echo '<label class="badge badge-warning">Sedang dalam proses pengujian</label>';
										} else {
											if ($get['status_uji'] == 'Belum Selesai') {
												echo '<label class="badge badge-danger">Belum Selesai</label>';
											} else {
												echo '<label class="badge badge-success">Selesai</label>';
											}
										} ?></td>
									<td><?= $get['tgl_perkiraan'] ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!--End row-->
	</div>
</div><!-- end app-content-->
</div>
<script>
	function filter() {
		var year = $("[name=year]").val();
		var month = $("[name=month]").val();
		window.location = "<?= base_url() ?>dashboard/index?year=" + year + "&month=" + month; // redirect
		return false;
	}
	$(function() {
		// bind change event to select
		$('[name=year]').on('change', function() {
			filter();
		});
		$('[name=month]').on('change', function() {
			filter();
		});
	});
</script>