<div class="app-content page-body">
	<div class="container">

		<!--Page header-->
		<div class="page-header">
			<div class="page-leftheader">
				<h4 class="page-title">Dashboard Jasa Teknis Industri</h4>
				<ol class="breadcrumb pl-0">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Dashboard Jasa Teknis Industri</li>
				</ol>
			</div>
			<div class="page-rightheader">
				<div class="ml-3 ml-auto d-flex">
					<div class="">
						<div class="border-right pr-4 mt-2 d-xl-block">
							<p class="text-muted mb-1">Category</p>
							<h6 class="font-weight-semibold mb-0">Jasa Teknis Industri</h6>
						</div>
					</div>
					<div class="">
						<div class="pl-4 pr-4 mt-2 d-xl-block">
							<p class="text-muted mb-0">Customer Rating</p>
							<div class="wideget-user-rating">
								<a href="#">
									<i class="fa fa-star text-warning"></i>
								</a>
								<a href="#">
									<i class="fa fa-star text-warning"></i>
								</a>
								<a href="#">
									<i class="fa fa-star text-warning"></i>
								</a>
								<a href="#">
									<i class="fa fa-star text-warning"></i>
								</a>
								<a href="#">
									<i class="fa fa-star-o text-warning mr-1"></i>
								</a>
								<span class="">(4.5/5)</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End Page header-->

		<!--Row-->
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="form-group">
					<label>Tahun</label>
					<select name="year" class="form-control custom-select" required>
						<?php for ($i = date('Y'); $i >= 2002; $i--) { ?>
							<option value="<?= $i ?>" <?= ($i == $year) ? 'selected' : ''; ?>><?= $i ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
				<div class="form-group">
					<label>Bulan</label>
					<select name="month" class="form-control custom-select" required>
						<?php for ($i = 1; $i <= 12; $i++) { ?>
							<option value="<?= $i ?>" <?= $i == $month ? 'selected' : ''; ?>><?= month_indo($i) ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
				<a href="#!" data-toggle="modal" data-target="#pnbpModal" data-kategori="pengujian" data-url="detail_pengujian">
					<div class="card overflow-hidden">
						<div class="card-body pb-0">
							<div class="text-left mb-4">
								<p class=" mb-1">Total PNBP Pengujian</p>
								<h2 class="mb-0 font-weight-semibold">Rp. <?= number_format($pnbp['pengujian']) ?></h2>
							</div>
						</div>
						<div id="spark1"></div>
					</div>
				</a>
			</div>
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
				<a href="#!" data-toggle="modal" data-target="#pnbpModal" data-kategori="lingkungan" data-url="detail_lingkungan">
					<div class="card overflow-hidden">
						<div class="card-body pb-0">
							<div class="text-left mb-4">
								<p class=" mb-1">Total PNBP Lingkungan</p>
								<h2 class="mb-0 font-weight-semibold">Rp. <?= number_format($pnbp['lingkungan']) ?></h2>
							</div>
						</div>
						<div id="spark2"></div>
					</div>
				</a>
			</div>
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
				<a href="#!" data-toggle="modal" data-target="#pnbpModal" data-kategori="kalibrasi" data-url="detail_kalibrasi">
					<div class="card overflow-hidden">
						<div class="card-body pb-0">
							<div class="text-left mb-4">
								<p class=" mb-1">Total PNBP Kalibrasi</p>
								<h2 class="mb-0 font-weight-semibold">Rp. <?= number_format($pnbp['kalibrasi']) ?></h2>
							</div>
						</div>
						<div id="spark3"></div>
					</div>
				</a>
			</div>
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
				<a href="#!" data-toggle="modal" data-target="#pnbpModal" data-kategori="sertifikasi-produk" data-url="detail_sertifikasi_produk">
					<div class="card overflow-hidden">
						<div class="card-body pb-0">
							<div class="text-left mb-4">
								<p class=" mb-1">Total PNBP Sertifikasi Produk</p>
								<h2 class="mb-0 font-weight-semibold">Rp. <?= number_format($pnbp['sertifikasi-produk']) ?></h2>
							</div>
						</div>
						<div id="spark4"></div>
					</div>
				</a>
			</div>
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
				<a href="#!" data-toggle="modal" data-target="#pnbpModal" data-kategori="sistem-mutu" data-url="detail_sertifikasi_sistem_mutu">
					<div class="card overflow-hidden">
						<div class="card-body pb-0">
							<div class="text-left mb-4">
								<p class=" mb-1">Total PNBP Sistem Mutu</p>
								<h2 class="mb-0 font-weight-semibold">Rp. <?= number_format($pnbp['sistem-mutu']) ?></h2>
							</div>
						</div>
						<div id="spark5"></div>
					</div>
				</a>
			</div>
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
				<a href="#!" data-toggle="modal" data-target="#pnbpModal" data-kategori="konsultasi" data-url="detail_konsultasi">
					<div class="card overflow-hidden">
						<div class="card-body pb-0">
							<div class="text-left mb-4">
								<p class=" mb-1">Total PNBP Konsultasi</p>
								<h2 class="mb-0 font-weight-semibold">Rp. <?= number_format($pnbp['konsultasi']) ?></h2>
							</div>
						</div>
						<div id="spark6"></div>
					</div>
				</a>
			</div>
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
				<a href="#!" data-toggle="modal" data-target="#pnbpModal" data-kategori="pelatihan" data-url="detail_pelatihan">
					<div class="card overflow-hidden">
						<div class="card-body pb-0">
							<div class="text-left mb-4">
								<p class=" mb-1">Total PNBP Pelatihan</p>
								<h2 class="mb-0 font-weight-semibold">Rp. <?= number_format($pnbp['pelatihan']) ?></h2>
							</div>
						</div>
						<div id="spark7"></div>
					</div>
				</a>
			</div>
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
				<a href="#!" data-toggle="modal" data-target="#pnbpModal" data-kategori="rbpi" data-url="detail_rbpi">
					<div class="card overflow-hidden">
						<div class="card-body pb-0">
							<div class="text-left mb-4">
								<p class=" mb-1">Total PNBP RBPI</p>
								<h2 class="mb-0 font-weight-semibold">Rp. <?= number_format($pnbp['rbpi']) ?></h2>
							</div>
						</div>
						<div id="spark8"></div>
					</div>
				</a>
			</div>
			<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
				<a href="#!" data-toggle="modal" data-target="#pnbpModal" data-kategori="teknologi-proses" data-url="detail_teknologi_proses">
					<div class="card overflow-hidden">
						<div class="card-body pb-0">
							<div class="text-left mb-4">
								<p class=" mb-1">Total Teknologi Proses</p>
								<h2 class="mb-0 font-weight-semibold">Rp. <?= number_format($pnbp['teknologi-proses']) ?></h2>
							</div>
						</div>
						<div id="spark9"></div>
					</div>
				</a>
			</div>
		</div>
		<!--End row-->

		<!--Row-->
		<div class="row">
			<div class="col-xl-6 col-md-12 col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title mb-0">Total PNBP</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12">
								<h6 class="mb-2">Target dan Realisasi Anggaran PNBP</h6>
								<h2 class="text-left mb-0 font-weight-semibold">Rp. <?= number_format($realisasi_pnbp) ?><span class="fs-12 text-left"><span class="text-success mr-1"><i class="fe fe-arrow-up ml-1 "></i> 12.5%</span>increased</span></h2>
							</div>
						</div>
						<p class="mt-1 text-muted">Berikut total PNBP untuk akumulasi di bulan <?= month_indo($month) ?> <?= $year ?>.</p>
						<div class="row mt-auto">
							<div class="col-md-6">
								<div class="">
									<h4 class="font-weight-semibold mb-1">Rp <?= number_format($target_pnbp) ?></h4>
									<p class="mb-0">Target</p>
								</div>
								<div class="progress progress-xs mt-1 mb-1 h-1">
									<div class="progress-bar bg-primary w-<?= percentRound10($percentage_pnbp) ?> " role="progressbar"></div>
								</div>
								<small class="text-muted"><i class="ti-arrow-up text-success mr-2"></i><?= number_format($percentage_pnbp) ?>% increase</small>
							</div>
							<div class="col-md-6">
								<div class="">
									<h4 class="font-weight-semibold mb-1"><?= number_format($percentage_pnbp) ?>%</h4>
									<p class="mb-0">Persentasi Realisasi</p>
								</div>
								<div class="progress progress-xs mt-1 mb-1 h-1">
									<div class="progress-bar bg-danger w-<?= percentRound10($percentage_pnbp) ?>" role="progressbar"></div>
								</div>
								<small class="text-muted"><i class="ti-arrow-up text-success mr-2"></i><?= number_format($percentage_pnbp) ?>% increase</small>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-6 col-md-12 col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title mb-0">Target dan Realisasi Anggaran RM</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12">
								<h6 class="mb-2">Total RM</h6>
								<h2 class="text-left mb-0 font-weight-semibold">Rp. <?= number_format($realisasi_rm) ?><span class="fs-12 text-left"><span class="text-success mr-1"><i class="fe fe-arrow-up ml-1 "></i> 12.5%</span>increased</span></h2>
							</div>
						</div>
						<p class="mt-1 text-muted">Berikut total RM untuk akumulasi di bulan <?= month_indo($month) ?> <?= $year ?>.</p>
						<div class="row mt-auto">
							<div class="col-md-6">
								<div class="">
									<h4 class="font-weight-semibold mb-1">Rp <?= number_format($target_rm) ?></h4>
									<p class="mb-0">Target</p>
								</div>
								<div class="progress progress-xs mt-1 mb-1 h-1">
									<div class="progress-bar bg-primary w-<?= percentRound10($percentage_rm) ?> " role="progressbar"></div>
								</div>
								<small class="text-muted"><i class="ti-arrow-up text-success mr-2"></i><?= number_format($percentage_rm) ?>% increase</small>
							</div>
							<div class="col-md-6">
								<div class="">
									<h4 class="font-weight-semibold mb-1"><?= number_format($percentage_rm) ?>%</h4>
									<p class="mb-0">Persentasi Realisasi</p>
								</div>
								<div class="progress progress-xs mt-1 mb-1 h-1">
									<div class="progress-bar bg-danger w-<?= percentRound10($percentage_rm) ?>" role="progressbar"></div>
								</div>
								<small class="text-muted"><i class="ti-arrow-up text-success mr-2"></i><?= number_format($percentage_rm) ?>% increase</small>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="col-xl-4 col-md-4 col-lg-4">
						<div class="card">
							<div class="card-header custom-header pb-0">
								<div>
									<h3 class="card-title ">Indeks Kepuasan Masyarakat</h3>
								</div>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-8">
										<h6 class="text-body text-uppercase font-weight-semibold">IKP</h6>
										<div class="wideget-user-rating">
											<?php $star = 5;
											for ($i = 1; $i <= $star; $i++) { ?>
												<a href="#">
													<i class="fa fa-<?= ($i <= $realisasi_ikm) ? 'star' : 'star-o' ?> text-warning"></i>
												</a>
											<?php } ?>
											<span class="">(<?= $realisasi_ikm ?>/5)</span>
										</div>
									</div>
								</div>
								<p class="text-muted mt-3">Data diperoleh dengan keterangan di bawah ini</p>
								<ul class="list-items">
									<li class="mb-2"> <span class="list-label ">Target</span> <span class="list-items-percentage float-right font-weight-bold"><?= $target_ikm ?></span> </li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-4 col-lg-4">
						<div class="card">
							<div class="card-header custom-header pb-0">
								<div>
									<h3 class="card-title ">Indeks Persepsi Korupsi</h3>
								</div>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-8">
										<h6 class="text-body text-uppercase font-weight-semibold">IPK</h6>
										<div class="wideget-user-rating">
											<?php $star = 5;
											for ($i = 1; $i <= $star; $i++) { ?>
												<a href="#">
													<i class="fa fa-<?= ($i <= $realisasi_ipk) ? 'star' : 'star-o' ?> text-warning"></i>
												</a>
											<?php } ?>
											<span class="">(<?= $realisasi_ipk ?>/5)</span>
										</div>
									</div>
								</div>
								<p class="text-muted mt-3">Data diperoleh dengan keterangan di bawah ini</p>
								<ul class="list-items">
									<li class="mb-2"> <span class="list-label ">Target</span> <span class="list-items-percentage float-right font-weight-bold"><?= $target_ipk ?></span> </li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-4 col-lg-4">
						<div class="card">
							<div class="card-header custom-header pb-0">
								<div>
									<h3 class="card-title ">Pemenuhan Standard Waktu Layanan Total</h3>
								</div>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-8">
										<h6 class="text-body text-uppercase font-weight-semibold">Pemenuhan Standard</h6>
										<h2 class="count mt-0 font-weight-semibold mb-0"><?= $realisasi_pemenuhan_standard ?>%</h2>
									</div>
									<div class="col-4">
										<p class="data-attributes  mb-0">
											<span class="donut" data-peity='{ "fill": ["#4a32d4", "rgba(255,255,255,0.3)"]}'><?= $realisasi_pemenuhan_standard ?>/100</span>
										</p>
									</div>
								</div>
								<p class="text-muted mt-3">Data diperoleh dengan keterangan di bawah ini</p>
								<ul class="list-items">
									<li class="mb-2"> <span class="list-label ">Target</span> <span class="list-items-percentage float-right font-weight-bold"><?= $target_pemenuhan_standard ?>%</span> </li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-4 col-lg-4">
						<div class="card">
							<div class="card-header custom-header pb-0">
								<div>
									<h3 class="card-title ">Persentase Komplain yang Diselesaikan</h3>
								</div>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-8">
										<h6 class="text-body text-uppercase font-weight-semibold">Persentase Komplain</h6>
										<h2 class="count mt-0 font-weight-semibold mb-0"><?= $realisasi_persentase_komplain ?>%</h2>
									</div>
									<div class="col-4">
										<p class="data-attributes  mb-0">
											<span class="donut" data-peity='{ "fill": ["#4a32d4", "rgba(255,255,255,0.3)"]}'><?= $realisasi_persentase_komplain ?>/100</span>
										</p>
									</div>
								</div>
								<p class="text-muted mt-3">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-4 col-lg-4">
						<div class="card">
							<div class="card-header custom-header pb-0">
								<div>
									<h3 class="card-title ">Jumlah Work Order</h3>
								</div>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-8">
										<h6 class="text-body text-uppercase font-weight-semibold">Jumlah WO</h6>
										<h2 class="count mt-0 font-weight-semibold mb-0"><?= ($realisasi_wo) ?></h2>
									</div>
									<div class="col-4">
										<p class="data-attributes  mb-0">
											<span class="donut" data-peity='{ "fill": ["#eab407", "rgba(255,255,255,0.3)"]}'>60/100</span>
										</p>
									</div>
								</div>
								<p class="text-muted mt-3">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-md-4 col-lg-4">
						<div class="card">
							<div class="card-header custom-header pb-0">
								<div>
									<h3 class="card-title ">Jumlah Customer Total</h3>
								</div>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-8">
										<h6 class="text-body text-uppercase font-weight-semibold">Jumlah Customer</h6>
										<h2 class="count mt-0 font-weight-semibold mb-0"><?= number_format($realisasi_customer) ?></h2>
									</div>
								</div>
								<p class="text-muted mt-3">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End row-->

		<!--Row-->
		<div class="row">
			<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title ">Jumlah PNBP Per Bulan</h3>
						<div class="card-options ">
							<a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
							<a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
						</div>
					</div>
					<div class="card-body text-center">
						<div class="row">
							<div class="col-xl-10">
								<canvas id="bar-chart"></canvas>
							</div>
							<div class="col-xl-2">
								<div class="mb-5">
									<h3 class="font-weight-semibold mb-0"><span class="dot-label bg-primary"></span><?= number_format($realisasi_pnbp) ?></h3>
									<p class="text-muted mb-2">Total PNBP</p>
								</div>
								<div class="mb-5">
									<h3 class="font-weight-semibold mb-0"><span class="dot-label bg-primary-50"></span><?= number_format($target_pnbp) ?></h3>
									<p class="text-muted mb-2">Target PNBP</p>
								</div>
								<div class="">
									<h3 class="font-weight-semibold mb-0"><span class="dot-label bg-primary-20"></span><?= number_format($sisa_pnbp) ?></h3>
									<p class="text-muted mb-2">Sisa Target</p>
								</div>
								<small class="mt-3 text-muted"><span class="font-weight-semibold">Data Terakhir :</span> <?= month_indo($month) ?> <?= $year ?></small>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div><!-- end app-content-->
</div>

<!-- Modal -->
<div class="modal fade" id="pnbpModal" tabindex="-1" role="dialog" aria-labelledby="pnbpModal" aria-hidden="true">
	<div class="modal-dialog modal-full" role="document">
		<div class="modal-content">
			<div id="data-pnbp"></div>
		</div>
	</div>
</div>
<script>
	$("#pnbpModal").on('show.bs.modal', function(e) {
		var kategori = $(e.relatedTarget).data('kategori');
		var year = $("[name=year]").val();
		var month = $("[name=month]").val();
		var url = $(e.relatedTarget).data('url');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>dashboard/" + url,
			data: {
				kategori: kategori,
				year: year,
				month: month,
			}
		}).done(function(response) {
			if (response != 'error') {
				$("#data-pnbp").html(response);
			} else {
				alert('Error');
			}
		});
	})

	function filter() {
		var year = $("[name=year]").val();
		var month = $("[name=month]").val();
		window.location = "<?= base_url() ?>dashboard/index?year=" + year + "&month=" + month; // redirect
		return false;
	}
	$(function() {
		// bind change event to select
		$('[name=year]').on('change', function() {
			filter();
		});
		$('[name=month]').on('change', function() {
			filter();
		});
	});
</script>