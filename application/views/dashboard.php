<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>

		<!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta content="Dashboard - Balai Besar Tekstil" name="description">
		<meta content="Balai Besar Tekstil" name="author">
		<meta name="keywords" content="dashboard bbt, dashboard, bbt, balai besar tekstil, dashboard balai besar tekstil, dash bbt, bbt dashboard, dashboard bbt indonesia, bbt indonesia"/>

		<!-- Title -->
		<title>Dashboard - Balai Besar Tekstil</title>

		<!--Favicon -->
		<link rel="icon" href="<?=base_url()?>assets/images/brand/favicon.ico" type="image/x-icon"/>

		<!-- Style css -->
		<link href="<?=base_url()?>assets/css/style.css" rel="stylesheet" />

		<!--Horizontal css -->
        <link id="effect" href="<?=base_url()?>assets/plugins/horizontal-menu/dropdown-effects/fade-up.css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/plugins/horizontal-menu/horizontal.css" rel="stylesheet" />

		<!-- P-scroll bar css-->
		<link href="<?=base_url()?>assets/plugins/p-scroll/perfect-scrollbar.css" rel="stylesheet" />

		<!---Icons css-->
		<link href="<?=base_url()?>assets/plugins/iconfonts/icons.css" rel="stylesheet" />
		<link href="<?=base_url()?>assets/plugins/iconfonts/font-awesome/font-awesome.min.css" rel="stylesheet">
		<link href="<?=base_url()?>assets/plugins/iconfonts/plugin.css" rel="stylesheet" />

		<!-- Skin css-->
		<link id="theme" rel="stylesheet" type="text/css" media="all" href="<?=base_url()?>assets/skins/hor-skin/horizontal-dark.css" />

	</head>

	<body class="app">

		<!---Global-loader-->
		<div id="global-loader" >
			<img src="<?=base_url()?>assets/images/svgs/loader.svg" alt="loader">
		</div>

		<div class="page">
			<div class="page-main">
				<div class="header top-header">
					<div class="container">
						<div class="d-flex">
							<a id="horizontal-navtoggle" class="animated-arrow hor-toggle"><span></span></a><!-- sidebar-toggle-->
							<a class="header-brand" href="index.html">
								<img src="<?=base_url()?>assets/images/logo.png" class="header-brand-img desktop-lgo" alt="Aronox logo">
								<img src="<?=base_url()?>assets/images/logo.png" class="header-brand-img mobile-logo" alt="Aronox logo">
							</a>
							<div class="d-flex order-lg-2 ml-auto">
								<a href="#" data-toggle="search" class="nav-link nav-link-lg d-md-none navsearch"><i class="fa fa-search"></i></a>
								<div class="dropdown   header-fullscreen" >
									<a  class="nav-link icon full-screen-link"  id="fullscreen-button">
										<i class="mdi mdi-arrow-collapse"></i>
									</a>
								</div>
								<div class="dropdown d-md-flex message">
									<a class="nav-link icon text-center" data-toggle="dropdown">
										<i class="mdi mdi-email-outline"></i>
										<span class="nav-unread bg-danger pulse"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow w-300  pt-0">
									    <div class="dropdown-header mt-0 pt-0 border-bottom p-4">
											<h5 class="dropdown-title mb-1 font-weight-semibold text-drak">Messages</h5>
											<p class="dropdown-title-text subtext mb-0 pb-0 ">You have 4 unread messages</p>
										</div>
										<a href="#" class="dropdown-item d-flex pb-4 pt-4">
											<div class="avatar avatar-md  mr-3 d-block cover-image border-radius-4" data-image-src="<?=base_url()?>assets/images/users/5.jpg">
												<span class="avatar-status bg-green"></span>
											</div>
											<div>
											    <small class="dropdown-text">Madeleine</small>
												<p class="mb-0 fs-13 text-muted">Hey! there I' am available</p>
											</div>
										</a>
										<a href="#" class="dropdown-item d-flex pb-4 pt-4">
											<div class="avatar avatar-md  mr-3 d-block cover-image border-radius-4" data-image-src="<?=base_url()?>assets/images/users/8.jpg">
												<span class="avatar-status bg-red"></span>
											</div>
											<div>
												<small class="dropdown-text">Anthony</small>
												<p class="mb-0 fs-13 text-muted ">New product Launching</p>
											</div>
										</a>
										<a href="#" class="dropdown-item d-flex pb-4 pt-4">
											<div class="avatar avatar-md  mr-3 d-block cover-image border-radius-4" data-image-src="<?=base_url()?>assets/images/users/11.jpg">
												<span class="avatar-status bg-yellow"></span>
											</div>
											<div>
												<small class="dropdown-text">Olivia</small>
												<p class="mb-0 fs-13 text-muted">New Schedule Realease</p>
											</div>
										</a>
										<div class="dropdown-divider mt-0"></div>
										<a href="#" class="dropdown-item text-center">See all Messages</a>
									</div>
								</div><!-- MESSAGE-BOX -->
								<div class="dropdown header-notify">
									<a class="nav-link icon" data-toggle="dropdown">
										<i class="mdi mdi-bell-outline"></i>
										<span class=" bg-success pulse-success "></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow pt-0">
									  <div class="dropdown-header border-bottom p-4 pt-0 mb-3 w-270">
									        <div class="d-flex">
												<h5 class="dropdown-title float-left mb-1 font-weight-semibold text-drak">Notifications</h5>
												<a href="#" class="fe fe-align-justify text-right float-right ml-auto text-muted"></a>
											</div>
										</div>
										<a href="#" class="dropdown-item d-flex pb-2 pt-2">
											<div class="card box-shadow-0 mb-0">
												<div class="card-body p-3">
													<div class="notifyimg bg-gradient-primary border-radius-4">
														<i class="mdi mdi-email-outline"></i>
													</div>
													<div>
														<div>Message Sent.</div>
														<div class="small text-muted">3 hours ago</div>
													</div>
												</div>
											</div>
										</a>
										<a href="#" class="dropdown-item d-flex  pb-2">
											<div class="card box-shadow-0 mb-0 ">
												<div class="card-body p-3">
													<div class="notifyimg bg-gradient-danger border-radius-4 bg-danger">
														<i class="fe fe-shopping-cart"></i>
													</div>
													<div>
														<div> Order Placed</div>
														<div class="small text-muted">5  hour ago</div>
													</div>
												</div>
											</div>
										</a>
										<a href="#" class="dropdown-item d-flex pb-2">
											<div class="card box-shadow-0 mb-0">
												<div class="card-body p-3">
													<div class="notifyimg bg-gradient-success  border-radius-4 bg-success mr-2">
														<i class="fe fe-airplay"></i>
													</div>
													<div>
														<div>Your Admin launched</div>
														<div class="small text-muted">1 daya ago</div>
													</div>
												</div>
											</div>
										</a>
										<div class=" text-center p-2 border-top mt-3">
											<a href="#" class="">View All Notifications</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Horizontal-menu -->
				<div class="horizontal-main hor-menu clearfix">
					<div class="horizontal-mainwrapper container clearfix">
						<nav class="horizontalMenu clearfix">
							<ul class="horizontalMenu-list">
								<li aria-haspopup="true"><a href="#" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i> Dashboard   <i class="fa fa-angle-down horizontal-icon"></i></a>
									<ul class="sub-menu">
										<li aria-haspopup="true"><a href="index.html">Ecommerce</a></li>
										<li aria-haspopup="true"><a href="index2.html">Analytics</a></li>
										<li aria-haspopup="true"><a href="index3.html">Finance</a></li>
										<li aria-haspopup="true"><a href="index4.html">Retail</a></li>
										<li aria-haspopup="true"><a href="index5.html">HR Dashboard</a></li>
										<li aria-haspopup="true"><a href="index6.html">Logistics</a></li>
										<li aria-haspopup="true"><a href="index7.html">Saas</a></li>
										<li aria-haspopup="true"><a href="index8.html">Performance</a></li>
										<li aria-haspopup="true"><a href="index9.html">Marketing</a></li>
										<li aria-haspopup="true"><a href="index10.html">Business</a></li>
									</ul>
								</li>
								<li aria-haspopup="true"><a href="#" class="sub-icon"><i class="typcn typcn-folder hor-icon"></i> Apps <i class="fa fa-angle-down horizontal-icon"></i></a>
									<ul class="sub-menu">
										<li aria-haspopup="true"><a href="cards.html">Cards design</a></li>
										<li aria-haspopup="true"><a href="cards-image.html">Image  Cards design</a></li>
										<li aria-haspopup="true"><a href="chat.html">Default Chat</a></li>
										<li aria-haspopup="true" class="sub-menu-sub"><a href="#">Calender</a>
											<ul class="sub-menu">
												<li aria-haspopup="true"><a href="calendar.html">Default calendar</a></li>
												<li aria-haspopup="true"><a href="calendar2.html">Full calendar</a></li>
											</ul>
										</li>
										<li aria-haspopup="true"><a href="notify.html">Notifications</a></li>
										<li aria-haspopup="true"><a href="sweetalert.html">Sweet alerts</a></li>
										<li aria-haspopup="true"><a href="rangeslider.html">Range slider</a></li>
										<li aria-haspopup="true"><a href="scroll.html">Content Scroll bar</a></li>
										<li aria-haspopup="true"><a href="counters.html">Counters</a></li>
										<li aria-haspopup="true"><a href="loaders.html">Loaders</a></li>
										<li aria-haspopup="true"><a href="time-line.html">Time Line</a></li>
										<li aria-haspopup="true"><a href="rating.html">Rating</a></li>
									</ul>
								</li>
								<li aria-haspopup="true"><a href="widgets.html" class=""><i class="typcn typcn-book hor-icon"></i> widgets</a></li>
								<li aria-haspopup="true"><a href="#" class="sub-icon"><i class="typcn typcn-chart-pie  hor-icon"></i> Charts <i class="fa fa-angle-down horizontal-icon"></i></a>
									<ul class="sub-menu">
										<li aria-haspopup="true"><a href="chart-chartist.html">Chartjs Charts</a></li>
										<li aria-haspopup="true"><a href="chart-morris.html">Morris Charts</a></li>
										<li aria-haspopup="true"><a href="chart-peity.html">Pie Charts</a></li>
										<li aria-haspopup="true"><a href="chart-echart.html">Echart Charts</a></li>
										<li aria-haspopup="true"><a href="chart-flot.html">Flot Charts</a></li>
										<li aria-haspopup="true"><a href="chart-nvd3.html">Nvd3 Charts</a></li>
										<li aria-haspopup="true"><a href="chart-dygraph.html">Dygraph Charts</a></li>
										<li aria-haspopup="true"><a href="chart-c3.html">C3 Charts</a></li>
									</ul>
								</li>
								<li aria-haspopup="true"><a href="#" class="sub-icon"><i class="typcn typcn-document-text hor-icon"></i> Advanced UI     <i class="fa fa-angle-down horizontal-icon"></i> </a>
									<ul class="sub-menu">
										<li aria-haspopup="true"><a href="modal.html">Modal</a></li>
										<li aria-haspopup="true"><a href="tooltipandpopover.html">Tooltip and popover</a></li>
										<li aria-haspopup="true"><a href="progress.html">Progress</a></li>
										<li aria-haspopup="true"><a href="chart.html">Charts</a></li>
										<li aria-haspopup="true"><a href="carousel.html">Carousels</a></li>
										<li aria-haspopup="true"><a href="accordion.html">Accordions</a></li>
										<li aria-haspopup="true"><a href="tabs.html">Tabs</a></li>
										<li aria-haspopup="true"><a href="headers.html">Headers</a></li>
										<li aria-haspopup="true"><a href="footers.html">Footers</a></li>
										<li aria-haspopup="true"><a href="crypto-currencies.html">Crypto-currencies</a></li>
										<li aria-haspopup="true"><a href="users-list.html">User List</a></li>
										<li aria-haspopup="true"><a href="search.html">Search page</a></li>
									</ul>
								</li>
								<li aria-haspopup="true"><a href="#" class="sub-icon"><i class="typcn typcn-edit hor-icon"></i> Elements  <i class="fa fa-angle-down horizontal-icon"></i></a>
									<div class="horizontal-megamenu clearfix">
										<div class="container">
											<div class="mega-menubg">
												<div class="row">
													<div class="col-lg-3 col-md-12 col-xs-12 link-list">
														<ul>
															<li><a href="alerts.html">Alerts</a></li>
															<li><a href="buttons.html">Buttons</a></li>
															<li><a href="colors.html">Colors</a></li>
															<li><a href="avatars.html"> Avatars</a></li>
															<li><a href="dropdown.html">Drop downs</a></li>
															<li><a href="thumbnails.html">Thumbnails</a></li>
															<li><a href="mediaobject.html">Media Object</a></li>
														</ul>
													</div>
													<div class="col-lg-3 col-md-12 col-xs-12 link-list">
														<ul>
															<li><a href="list.html">List</a></li>
															<li><a href="tags.html">Tags</a></li>
															<li><a href="pagination.html">Pagination</a></li>
															<li><a href="navigation.html">Navigation</a></li>
															<li><a href="typography.html">Typography</a></li>
															<li><a href="breadcrumbs.html"> Breadcrumbs</a></li>
															<li><a href="invoice.html">Invoice</a></li>
														</ul>
													</div>
													<div class="col-lg-3 col-md-12 col-xs-12 link-list">
														<ul>
															<li><a href="badge.html">Badges</a></li>
															<li><a href="jumbotron.html">Jumbotron</a></li>
															<li><a href="panels.html">Panels</a></li>
															<li><a href="profile.html">Profile</a></li>
															<li><a href="editprofile.html">Edit Profile</a></li>
															<li><a href="pricing.html">Pricing Tables</a></li>
															<li><a href="gallery.html">Gallery</a></li>
														</ul>
													</div>
													<div class="col-lg-3 col-md-12 col-xs-12 link-list">
														<ul>
															<li><a href="emailservices.html"> Email</a></li>
															<li><a href="email.html">Email Inbox</a></li>
															<li><a href="about.html">About Company</a></li>
															<li><a href="services.html">Services</a></li>
															<li><a href="faq.html">FAQS</a></li>
															<li><a href="terms.html">Terms and Conditions</a></li>
															<li><a href="blog.html">Blog</a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li aria-haspopup="true"><a href="#" class="sub-icon "><i class="typcn typcn-tabs-outline hor-icon"></i> Pages <i class="fa fa-angle-down horizontal-icon"></i></a>
									<ul class="sub-menu">
										<li><a href="maps.html">Vector Maps</a></li>
										<li><a href="empty.html">Empty Page</a></li>
										<li aria-haspopup="true" class="sub-menu-sub"><a href="#">Tables</a>
											<ul class="sub-menu">
												<li aria-haspopup="true"><a href="tables.html">Default table</a></li>
												<li aria-haspopup="true"><a href="datatable.html">Data Table</a></li>
											</ul>
										</li>
										<li aria-haspopup="true" class="sub-menu-sub"><a href="#">Forms</a>
											<ul class="sub-menu">
												<li aria-haspopup="true"><a href="form-elements.html">Form Elements</a></li>
												<li aria-haspopup="true"><a href="form-wizard.html">Form-wizard Elements</a></li>
												<li aria-haspopup="true"><a href="wysiwyag.html">Text Editor</a></li>
											</ul>
										</li>
										<li aria-haspopup="true" class="sub-menu-sub"><a href="#">E-commerce</a>
											<ul class="sub-menu">
												<li aria-haspopup="true"><a href="shop.html">Products</a></li>
												<li aria-haspopup="true"><a href="shop-des.html">Product Details</a></li>
												<li aria-haspopup="true"><a href="cart.html">Shopping Cart</a></li>
											</ul>
										</li>
										<li aria-haspopup="true" class="sub-menu-sub"><a href="#">Account</a>
											<ul class="sub-menu">
												<li aria-haspopup="true"><a href="login.html">Login</a></li>
												<li aria-haspopup="true"><a href="register.html">Register</a></li>
												<li aria-haspopup="true"><a href="forgot-password.html">Forgot password</a></li>
												<li aria-haspopup="true"><a href="lockscreen.html">Lock screen</a></li>
											</ul>
										</li>
										<li aria-haspopup="true" class="sub-menu-sub"><a href="#">Error Pages</a>
											<ul class="sub-menu">
												<li aria-haspopup="true"><a href="400.html">400 Error</a></li>
												<li aria-haspopup="true"><a href="401.html">401 Error</a></li>
												<li aria-haspopup="true"><a href="403.html">403 Error</a></li>
												<li aria-haspopup="true"><a href="404.html">404 Error</a></li>
												<li aria-haspopup="true"><a href="500.html">500 Error</a></li>
												<li aria-haspopup="true"><a href="503.html">503 Error</a></li>
											</ul>
										</li>
										<li><a href="construction.html">Under Construction</a></li>
									</ul>
								</li>
								<li aria-haspopup="true"><a href="#" class="sub-icon"><i class="typcn typcn-cog-outline hor-icon"></i> Icons <i class="fa fa-angle-down horizontal-icon"></i></a>
									<ul class="sub-menu">
										<li aria-haspopup="true"><a href="icons.html">Font Awesome</a></li>
										<li aria-haspopup="true"><a href="icons2.html">Material Design Icons</a></li>
										<li aria-haspopup="true"><a href="icons3.html">Simple line Icons</a></li>
										<li aria-haspopup="true"><a href="icons4.html">Feather Icons</a></li>
										<li aria-haspopup="true"><a href="icons5.html">Ionic Icons</a></li>
										<li aria-haspopup="true"><a href="icons6.html"> Flag Icons</a></li>
										<li aria-haspopup="true"><a href="icons7.html">pe7 Icons</a></li>
										<li aria-haspopup="true"><a href="icons8.html">Themify Icons</a></li>
										<li aria-haspopup="true"><a href="icons9.html"> Typicons Icons</a></li>
										<li aria-haspopup="true"><a href="icons10.html">Weather Icons</a></li>
									</ul>
								</li>
							</ul>
						</nav>
						<!--Nav end -->
					</div>
				</div>
				<!-- Horizontal-menu end -->

				<div class="app-content page-body">
					<div class="container-fluid">

						<!--Row-->
						<div class="row">
							<div class="col-xl-12 col-md-12 col-lg-12">
								<div class="card">
                                    <div class="card-body">
										<div class="row">
											<div class=" col-xl col-sm-6 d-flex mb-5 mb-xl-0">
												<div class="feature ">
													<i class="si si-briefcase primary feature-icon bg-primary"></i>
												</div>
												<div class="ml-3">
													<small class=" mb-0">INDEKS KEPUASAN MASYARAKAT</small><br>
													<h3 class="font-weight-semibold mb-0">3.5</h3>
													<small class="mb-0 text-muted"><span class="text-success font-weight-semibold"><i class="fa fa-caret-up  mr-1"></i> 0.67%</span> Last year</small>
												</div>
											</div>
											<div class=" col-xl col-sm-6 d-flex mb-5 mb-xl-0">
												<div class="feature">
													<i class="si si-layers danger feature-icon bg-danger"></i>
												</div>
												<div class=" d-flex flex-column  ml-3"> <small class=" mb-0">PEMENUHAN STANDARD WAKTU LAYANAN</small>
													<h3 class="font-weight-semibold mb-0">2,536</h3>
													<small class="mb-0 text-muted"><span class="text-success font-weight-semibold"><i class="fa fa-caret-up  mr-1"></i> 0.25%</span> Last year</small>
												</div>
											</div>
											<div class=" col-xl col-sm-6 d-flex  mb-5 mb-sm-0">
												<div class="feature">
													<i class="si si-note secondary feature-icon bg-secondary"></i>
												</div>
												<div class=" d-flex flex-column ml-3"> <small class=" mb-0">PERSENTASE KOMPLAIN</small>
													<h3 class="font-weight-semibold mb-0">1</h3>
													<small class="mb-0 text-muted"><span class="text-success font-weight-semibold"><i class="fa fa-caret-up  mr-1"></i> 0.67%</span> Last year</small>
												</div>
											</div>
											<div class=" col-xl col-sm-6 d-flex">
												<div class="feature">
													<i class="si si-basket-loaded success feature-icon bg-success"></i>
												</div>
												<div class=" d-flex flex-column  ml-3"> <small class=" mb-0">JUMLAH WORK ORDER</small>
													<h3 class="font-weight-semibold mb-0">7,836</h3>
													<small class="mb-0 text-muted"><span class="text-danger font-weight-semibold"><i class="fa fa-caret-down  mr-1"></i> 0.32%</span> Last year</small>
												</div>
											</div>
											<div class=" col-xl col-sm-6 d-flex">
												<div class="feature">
													<i class="si si-basket-loaded secondary feature-icon bg-secondary"></i>
												</div>
												<div class=" d-flex flex-column  ml-3"> <small class=" mb-0">JUMLAH PENGADAAN SARANA PERKANTORAN</small>
													<h3 class="font-weight-semibold mb-0">7,836</h3>
													<small class="mb-0 text-muted"><span class="text-danger font-weight-semibold"><i class="fa fa-caret-down  mr-1"></i> 0.32%</span> Last year</small>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--End row-->

						<!--Row-->
						<div class="row">
							<div class="col-xl-5 col-md-12 col-lg-5">
								<div class="card">
								    <div class="card-header">
									    <h3 class="card-title mb-0">SDM Per Bulan</h3>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-4 text-center mb-4 mb-md-0">
												<p class="data-attributes mb-0">
													<span class="donut" data-peity='{ "fill": ["#4a32d4", "rgba(236, 246, 249, 0.1)"]}'>4/5</span>
												</p>
												<h4 class=" mb-0 font-weight-semibold">3,678</h4>
												<p class="mb-0 text-muted">JUMLAH PEGAWAI YANG MENDAPAT PELATIHAN</p>
											</div>
											<div class="col-md-4 text-center mb-4 mb-md-0">
												<p class="data-attributes mb-0">
													<span class="donut" data-peity='{ "fill": ["#fb1c52", "rgba(236, 246, 249, 0.2)"]}'>226/360</span>
												</p>
												<h4 class=" mb-0 font-weight-semibold">398</h4>
												<p class="mb-0 text-muted">KOMPOSISI SDM</p>
											</div>
											<div class="col-md-4 text-center">
												<p class="data-attributes mb-0">
													<span class="donut" data-peity='{ "fill": ["#f7be2d", "rgba(236, 246, 249, 0.2)"]}'>4,4</span>
												</p>
												<h4 class=" mb-0 font-weight-semibold">289</h4>
												<p class="mb-0 text-muted">PEGAWAI YANG TELAT</p>
											</div>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-body">
										<div class=" ">
											<h5>Total PNBP of this year</h5>
										</div>
										<h2 class="mb-2 font-weight-semibold">Rp. 6.382.500<span class="sparkline_bar31 float-right"></span></h2>
										<div class="text-muted mb-0">
											<i class="fa fa-arrow-circle-o-up text-success"></i>
											<span>12.3% higher vs previous year</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-7 col-md-12 col-lg-7">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">PNBP Tiap Layanan</h3>
										<div class="card-options ">
											<a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
											<a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
										</div>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-xl-4 col-lg-4 col-md-12 mb-5">
												<p class=" mb-0 "> Jumlah Alat Dikalibrasi</p>
												<h2 class="mb-0 font-weight-semibold">35,789<span class="fs-12 text-muted"><span class="text-danger mr-1"><i class="fe fe-arrow-down ml-1"></i>0.9%</span>last year</span></h2>
											</div>
											<div class="col-xl-4 col-lg-4 col-md-12 mb-5">
												<p class=" mb-0 ">Jumlah Sampel</p>
												<h2 class="mb-0 font-weight-semibold">$9,265<span class="fs-12 text-muted"><span class="text-success mr-1"><i class="fe fe-arrow-up ml-1"></i>0.15%</span>last year</span></h2>
											</div>
											<div class="col-xl-4 col-lg-4 col-md-12 mb-5">
												<p class=" mb-0 "> Jumlah Customer</p>
												<h2 class="mb-0 font-weight-semibold">120<span class="fs-12 text-muted"><span class="text-danger mr-1"><i class="fe fe-arrow-down ml-1"></i>1.04%</span>last year</span></h2>
											</div>
										</div>
										<div class="chart-wrapper">
											<canvas id="sales" class=" chartjs-render-monitor chart-dropshadow2 h-184"></canvas>
									    </div>
									</div>
								</div>
							</div>
						</div>
						<!--End row-->

						<!--Row-->
						<div class="row">
							<div class="col-xl-4 col-md-6 col-lg-6">
								<div class="card">
									<div class="card-header">
										<div class="card-title">Work Order</div>
										<div class="card-options ">
											<a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
											<a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
										</div>
									</div>
									<div class="card-body sm-p-0">
										<div class="list-group projects-list border pt-0 pb-0 pl-0 pr-0">
											<a href="#" class="list-group-item list-group-item-action flex-column align-items-start border-0">
												<div class="d-flex w-100 justify-content-between">
													<h6 class="mb-1 font-weight-semibold ">001/WO/BBT/X/2021</h6>
													<h6 class="mb-0 font-weight-semibold ">06/08/2021</h6>
												</div>
												<div class="d-flex w-100 justify-content-between">
													<span class="text-muted"><i class="fe fe-arrow-down text-success "></i> 03% last month</span>
													<span class="text-muted">today</span>
												</div>
											</a>
											<a href="#" class="list-group-item list-group-item-action flex-column align-items-start border-bottom-0  border-left-0 border-right-0 border-top">
												<div class="d-flex w-100 justify-content-between">
													<h6 class="mb-1 font-weight-semibold ">Storage</h6>
													<h6 class="mb-0 font-weight-semibold ">2,178</h6>
												</div>
												<div class="d-flex w-100 justify-content-between">
													<span class="text-muted"><i class="fe fe-arrow-down text-danger "></i> 16% last month</span>
													<span class="text-muted">2 days ago</span>
												</div>
											</a>
											<a href="#" class="list-group-item list-group-item-action flex-column align-items-start border-bottom-0  border-left-0 border-right-0 border-top">
												<div class="d-flex w-100 justify-content-between">
													<h6 class="mb-1 font-weight-semibold ">Shipping</h6>
													<h6 class="mb-0 font-weight-semibold ">1,367</h6>
												</div>
												<div class="d-flex w-100 justify-content-between">
													<span class="text-muted"><i class="fe fe-arrow-up text-success"></i> 06% last month</span>
													<span class="text-muted">1 days ago</span>
												</div>
											</a>
											<a href="#" class="list-group-item list-group-item-action flex-column align-items-start border-bottom-0  border-left-0 border-right-0 border-top">
												<div class="d-flex w-100 justify-content-between">
													<h6 class="mb-1 font-weight-semibold ">Receiving</h6>
													<h6 class="mb-0 font-weight-semibold ">678</h6>
												</div>
												<div class="d-flex w-100 justify-content-between">
													<span class="text-muted"><i class="fe fe-arrow-down text-danger "></i> 25% last month</span>
													<span class="text-muted">10 days ago</span>
												</div>
											</a>
											<a href="#" class="list-group-item list-group-item-action flex-column align-items-start border-bottom-0  border-left-0 border-right-0 border-top">
												<div class="d-flex w-100 justify-content-between">
													<h6 class="mb-1 font-weight-semibold ">Other</h6>
													<h6 class="mb-0 font-weight-semibold ">5,678</h6>
												</div>
												<div class="d-flex w-100 justify-content-between">
													<span class="text-muted"><i class="fe fe-arrow-up text-success "></i> 16% last month</span>
													<span class="text-muted">5 days ago</span>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 col-md-6 col-lg-6">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Data Alat BBT</h3>
										<div class="card-options ">
											<a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
											<a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
										</div>
									</div>
									<div class="card-body sm-p-2">
										<div class="rated-products">
											<ul class="vertical-scroll mb-0">
												<li class="item">
													<div class="media mt-0 pl-0 pr-0 mb-5">
														<img class="mr-4" src="<?=base_url()?>assets/images/products/4.png" alt="img">
														<div class="media-body">
															<h4 class="mt-0 mb-1 font-weight-sembold">Hand Bag</h4>
															<span class="text-muted">exclusive sale</span><br>
															<span class="rated-products-ratings">
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star-half-o text-warning"></i>
															</span>
														</div>
														<div class="h5 mb-0 font-weight-semibold mt-4 flot-right">2 Year</div>
													</div>
												</li>
												<li class="item">
													<div class="media mt-0 pl-0 pr-0 mb-5">
														<img class=" mr-4" src="<?=base_url()?>assets/images/products/8.png" alt="img">
														<div class="media-body">
															<h4 class="mb-1 font-weight-sembold">Sports Shoe</h4>
															<span class="text-muted">exclusive sale</span><br>
															<span class="rated-products-ratings">
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star-half-o text-warning"></i>
																<i class="fa fa-star-o text-warning"></i>
															</span>
														</div>
														<div class="h5 mb-0 font-weight-semibold mt-4 flot-right">$15.00</div>
													</div>
												</li>
												<li class="item">
													<div class="media mt-0 pl-0 pr-0 mb-5">
														<img class=" mr-4" src="<?=base_url()?>assets/images/products/9.png" alt="img">
														<div class="media-body">
															<h4 class="mb-1 font-weight-sembold">Mobile</h4>
															<span class="text-muted">exclusive sale</span><br>
															<span class="rated-products-ratings">
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star-o text-warning"></i>
																<i class="fa fa-star-o text-warning"></i>
															</span>
														</div>
														<div class="h5 mb-0 font-weight-semibold mt-4 flot-right">$30.00</div>
													</div>
												</li>
												<li class="item">
													<div class="media mt-0 pl-0 pr-0 mb-0">
														<img class=" mr-4" src="<?=base_url()?>assets/images/products/6.png" alt="img">
														<div class="media-body">
															<h4 class=" mb-1 font-weight-sembold">Laptop</h4>
															<span class="text-muted">exclusive sale</span><br>
															<span class="rated-products-ratings">
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star text-warning"></i>
																<i class="fa fa-star-o text-warning"></i>
																<i class="fa fa-star-o text-warning"></i>
																<i class="fa fa-star-o text-warning"></i>
															</span>
														</div>
														<div class="h5 mb-0 font-weight-semibold mt-4 flot-right">$42.00</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 col-md-12 col-lg-12">
								<div class="card">
									<div class="card-body">
										<p class=" mb-0 ">Order status</p>
										<h3 class="mb-0 font-weight-semibold">$5,69,265<span class="fs-12 text-muted"><span class="text-success mr-1"><i class="fe fe-arrow-up ml-1"></i>0.15%</span>last year</span></h3>
										<div class="row mt-3 text-center">
											<div class="col-md-4 border-right mb-4 mb-md-0">
												<p class="mb-0 text-muted">This Week</p>
												<h5 class="mb-0">34%</h5>
											</div>
											<div class="col-md-4 border-right mb-4 mb-md-0">
												<p class="mb-0 text-muted">Last Week</p>
												<h5 class="mb-0">67%</h5>
											</div>
											<div class="col-md-4">
												<p class="mb-0 text-muted">Total</p>
												<h5 class="mb-0">$63,456</h5>
											</div>
										</div>
									</div>
								</div>
								<div class="card overflow-hidden">
									<div class="card-body">
										<div class="row">
											<div class="col-md-8">
												<span class=" mb-1">Sales Revenue</span>
												<h3 class="mb-3  font-weight-semibold">$5,89,268<span class="fs-12 text-danger ml-1">last 6 months</span></h3>
												<p class="mb-0 text-muted">It is a long established fact that a reader will be distracted by the readable content of a page.</p>
											</div>
											<div class="col-md-4 mt-5">
												<div class="chart-circle chart-circle-sm  overflow-hiddene  mt-sm-0 mb-0 text-left" data-value="0.58" data-thickness="8" data-color="#4a32d4">
													<div class="chart-circle-value text-center">
														<h5 class="mb-0">65%</h5>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="spark1" class=""></div>
								</div>
							</div>
						</div>
						<!--End row-->

						<!--Row-->
						<div class="row">
							<div class="col-xl-3 col-sm-12 col-lg-6 col-md-6">
								<div class="card product-info-card">
									<div class="card-body d-flex flex-column">
										<div class="">
											<div class="team-img text-center">
												<img src="<?=base_url()?>assets/images/products/hand-bag.png" class="" alt="">
											</div>
											<div class="text-center mt-4">
												<div class="text-warning mt-1">
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star-half-full"> </i>
												</div>
											  <h4 class="font-weight-semibold mb-0">Womens Bags</h4>
											  <p class="mb-0 mt-1 text-muted"> exclusive sale</p>
											</div>
											<div class="d-flex justify-content-between mt-auto pt-5">
												<div class="product-item-wrap d-flex">
													<h4 class="mb-0 font-weight-semibold fs-20  mt-2 text-success">$49<span class="h5 text-muted ml-2"><del>$69</del></span></h4>
												</div>
												<button type="button" class="btn btn-primary-gradient"> Add Cart</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-sm-12 col-lg-6 col-md-6">
								<div class="card product-info-card">
									<div class="card-body d-flex flex-column">
										<div class="">
											<div class="team-img text-center">
												<img src="<?=base_url()?>assets/images/products/samsung.png" class="" alt="">
											</div>
											<div class="text-center mt-4">
												<div class="text-warning mt-1">
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star-half-full"> </i>
												</div>
											  <h4 class="font-weight-semibold mb-0">Digital Watch</h4>
											  <p class="mb-0 mt-1 text-muted"> exclusive sale</p>
											</div>
											<div class="d-flex justify-content-between mt-auto pt-5">
												<div class="product-item-wrap d-flex">
													<h4 class="mb-0 font-weight-semibold fs-20 mt-2 text-success">$120<span class="h5 text-muted ml-2"><del>$159</del></span></h4>
												</div>
												<button type="button" class="btn btn-primary-gradient">Add Cart</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-sm-12 col-lg-6 col-md-6">
								<div class="card product-info-card">
									<div class="card-body d-flex flex-column">
										<div class="">
											<div class="team-img text-center">
												<img src="<?=base_url()?>assets/images/products/mobile.png" class="" alt="">
											</div>
											<div class="text-center mt-4">
												<div class="text-warning mt-1">
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star-half-full"> </i>
												</div>
											  <h4 class="font-weight-semibold mb-0">Mobile Phone</h4>
											  <p class="mb-0 mt-1 text-muted">exclusive sale</p>
											</div>
											<div class="d-flex justify-content-between mt-auto pt-5">
												<div class="product-item-wrap d-flex">
													<h4 class="mb-0 font-weight-semibold fs-20 mt-2 text-success">$299<span class="h5 text-muted ml-2"><del>$359</del></span></h4>
												</div>
												<button type="button" class="btn btn-primary-gradient"> Add Cart</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-sm-12 col-lg-6 col-md-6">
								<div class="card product-info-card">
									<div class="card-body d-flex flex-column">
										<div class="">
											<div class="team-img text-center">
												<img src="<?=base_url()?>assets/images/products/headphones.png"  class="" alt="">
											</div>
											<div class="text-center mt-4">
												<div class="text-warning mt-1">
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star"> </i>
													<i class="fa fa-star-half-full"> </i>
												</div>
											  <h4 class="font-weight-semibold mb-0">Headphones</h4>
											  <p class="mb-0 mt-1 text-muted">exclusive sale</p>
											</div>
											<div class="d-flex justify-content-between mt-auto pt-5">
												<div class="product-item-wrap d-flex">
													<h4 class="mb-0 font-weight-semibold fs-20 mt-2 text-success">$39<span class="h5 text-muted ml-2"><del>$359</del></span></h4>
												</div>
												<button type="button" class="btn btn-primary-gradient"> Add Cart</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!--Row-->
						<div class="row">
							<div class="col-12 col-sm-12">
								<div class="card">
									<div class="card-header">
										<div class="card-title">Indikator Output Kegiatan Tahun Anggaran 2019 Balai Besar Tekstil</div>
										<div class="card-options ">
											<a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
											<a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
										</div>
									</div>
									<div class="card-body">
										<div class="table-responsive">
											<table class="table  table-vcenter border mb-0 text-nowrap">
												<thead class="">
													<tr>
														<th>Indikator Output Kegiatan</th>
														<th>Target</th>
														<th>Satuan</th>
														<th>Jan</th>
														<th>Feb</th>
														<th>Mar</th>
														<th>Apr</th>
														<th>May</th>
														<th>Jun</th>
														<th>Jul</th>
														<th>Aug</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="text-sm font-weight-600">A</td>
														<td>8</td>
														<td>Litbang</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
													</tr>
													<tr>
														<td class="text-sm font-weight-600">A</td>
														<td>8</td>
														<td>Litbang</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
													</tr>
													<tr>
														<td class="text-sm font-weight-600">A</td>
														<td>8</td>
														<td>Litbang</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
													</tr>
													<tr>
														<td class="text-sm font-weight-600">A</td>
														<td>8</td>
														<td>Litbang</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
													</tr>
													<tr>
														<td class="text-sm font-weight-600">A</td>
														<td>8</td>
														<td>Litbang</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
													</tr>
													<tr>
														<td class="text-sm font-weight-600">A</td>
														<td>8</td>
														<td>Litbang</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
													</tr>
													<tr>
														<td class="text-sm font-weight-600">A</td>
														<td>8</td>
														<td>Litbang</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
													</tr>
													<tr>
														<td class="text-sm font-weight-600">A</td>
														<td>8</td>
														<td>Litbang</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
														<td>0</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--End row-->
					</div>
				</div><!-- end app-content-->
			</div>

			<!--Footer-->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
							Copyright © 2019 <a href="#">Aronox</a>. Designed by <a href="#">Spruko Technologies Pvt.Ltd</a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->

		</div>

		<!-- Back to top -->
		<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

		<!-- Jquery js-->
		<script src="<?=base_url()?>assets/js/vendors/jquery-3.4.0.min.js"></script>

		<!-- Bootstrap4 js-->
		<script src="<?=base_url()?>assets/plugins/bootstrap/popper.min.js"></script>
		<script src="<?=base_url()?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>

		<!--Othercharts js-->
		<script src="<?=base_url()?>assets/plugins/othercharts/jquery.sparkline.min.js"></script>

		<!-- Circle-progress js-->
		<script src="<?=base_url()?>assets/js/vendors/circle-progress.min.js"></script>

		<!-- Jquery-rating js-->
		<script src="<?=base_url()?>assets/plugins/rating/jquery.rating-stars.js"></script>

		<!--Horizontal js-->
		<script src="<?=base_url()?>assets/plugins/horizontal-menu/horizontal.js"></script>

		<!-- P-scroll js-->
		<script src="<?=base_url()?>assets/plugins/p-scroll/perfect-scrollbar.js"></script>

		<!-- ECharts js -->
		<script src="<?=base_url()?>assets/plugins/echarts/echarts.js"></script>

		<!-- CHARTJS CHART -->
		<script src="<?=base_url()?>assets/plugins/chart/chart.bundle.js"></script>
		<script src="<?=base_url()?>assets/plugins/chart/utils.js"></script>

		<!-- Peitychart js-->
		<script src="<?=base_url()?>assets/plugins/peitychart/jquery.peity.min.js"></script>
		<script src="<?=base_url()?>assets/plugins/peitychart/peitychart.init.js"></script>

		<!-- Index js-->
		<script src="<?=base_url()?>assets/js/index1.js"></script>

		<!-- Apexchart js-->
		<script src="<?=base_url()?>assets/js/apexcharts.js"></script>

		<!-- Custom js-->
		<script src="<?=base_url()?>assets/js/custom-dark.js"></script>


	</body>
</html>