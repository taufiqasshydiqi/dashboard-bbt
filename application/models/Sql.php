<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sql extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function get($table, $where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get($table);

        return $query;
    }

    public function create($table, $data)
    {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function update($table, $data, $where)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete($table, $where)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function query($query)
    {
        return $this->db->query($query);
    }
}
