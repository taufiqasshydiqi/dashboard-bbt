<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_custom extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function pnbp($where = null, $year = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("a.layanan_id, a.slug, a.nama_layanan, b.pnbp_id, b.target, b.tahun");
        $this->db->from("tbl_layanan a");
        if (isset($year)) {
            $this->db->join("tbl_pnbp b", "a.layanan_id = b.layanan_id AND b.tahun = '" . $year . "'", "left outer");
        } else {
            $this->db->join("tbl_pnbp b", "a.layanan_id = b.layanan_id", "left outer");
        }
        $query = $this->db->get();

        return $query;
    }

    function pnbp_detail($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("a.*, b.*");
        $this->db->from('tbl_pnbp a');
        $this->db->join('tbl_pnbp_detail b', 'a.pnbp_id = b.pnbp_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function pnbp_dashboard($year, $month)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("a.layanan_id, a.slug, a.nama_layanan, b.pnbp_id, b.target, b.tahun, c.hasil");
        $this->db->from("tbl_layanan a");
        $this->db->join("tbl_pnbp b", "a.layanan_id = b.layanan_id AND b.tahun = '" . $year . "'", "left outer");
        $this->db->join('tbl_pnbp_detail c', "b.pnbp_id = c.pnbp_id AND c.bulan = '" . $month . "'", 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function ikm($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("*");
        $this->db->from('tbl_ikm a');
        $this->db->join('tbl_ikm_detail b', 'a.ikm_id = b.ikm_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function ipk($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("*");
        $this->db->from('tbl_ipk a');
        $this->db->join('tbl_ipk_detail b', 'a.ipk_id = b.ipk_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function pemenuhan_standard($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("*");
        $this->db->from('tbl_pemenuhan_standard a');
        $this->db->join('tbl_pemenuhan_standard_detail b', 'a.pemenuhan_standard_id = b.pemenuhan_standard_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function persentase_komplain($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("*");
        $this->db->from('tbl_persentase_komplain a');
        $this->db->join('tbl_persentase_komplain_detail b', 'a.persentase_komplain_id = b.persentase_komplain_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function sample($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("*");
        $this->db->from('tbl_sample a');
        $this->db->join('tbl_sample_detail b', 'a.sample_id = b.sample_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function wo($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("*");
        $this->db->from('tbl_wo a');
        $this->db->join('tbl_wo_detail b', 'a.wo_id = b.wo_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function sasaran_mutu($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("*");
        $this->db->from('tbl_sasaran_mutu a');
        $this->db->join('tbl_sasaran_mutu_detail b', 'a.sasaran_mutu_id = b.sasaran_mutu_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function ketepatan_waktu($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("*");
        $this->db->from('tbl_ketepatan_waktu a');
        $this->db->join('tbl_ketepatan_waktu_detail b', 'a.ketepatan_waktu_id = b.ketepatan_waktu_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function anggaran_rm($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("*");
        $this->db->from('tbl_anggaran_rm a');
        $this->db->join('tbl_anggaran_rm_detail b', 'a.anggaran_rm_id = b.anggaran_rm_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function customer($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("*");
        $this->db->from('tbl_customer a');
        $this->db->join('tbl_customer_detail b', 'a.customer_id = b.customer_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    function customer_type($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("*");
        $this->db->from('tbl_layanan a');
        $this->db->join('tbl_customer_type b', 'a.layanan_id = b.layanan_id', 'left outer');
        $query = $this->db->get();

        return $query;
    }

    public function create($table, $data)
    {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function update($table, $data, $where)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function delete($table, $where)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function query($query)
    {
        return $this->db->query($query);
    }

    public function history()
    {
        $bbt = $this->load->database('bbt', TRUE);
        $q =     "SELECT 
					* 
				FROM 
					`pengujian_parameter` a 
				JOIN 
					`anggota` b 
						ON 
					a.`id_anggota` = b.`id_anggota` 
				JOIN
					`pemesanan` c
						ON
					a.`id_pemesanan` = c.`id_pemesanan`
				JOIN
					`parameter` d
						ON
					a.`id_parameter` = d.`id_parameter`
				WHERE 
					a.`id_anggota` != '' 
						AND
					b.`id_jabatan` IN (5,6)
				ORDER BY 
					a.`id_pengujian` DESC LIMIT 100";
        $r = $bbt->query($q, false)->result_array();

        return $r;
    }
}
