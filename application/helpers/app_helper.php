<?php
if (!function_exists('month_indo')) {
    function month_indo($value)
    {
        $month = [
            '1' => "Januari",
            '2' => "Februari",
            '3' => "Maret",
            '4' => "April",
            '5' => "Mei",
            '6' => "Juni",
            '7' => "Juli",
            '8' => "Agustus",
            '9' => "September",
            '10' => "Oktober",
            '11' => "November",
            '12' => "Desember"
        ];

        return $month[$value];
    }
}
if (!function_exists('percentRound10')) {
    function percentRound10($value)
    {
        switch (true) {
            case $value <= 10:
                return 10;
                break;
            case $value > 10 && $value <= 20:
                return 20;
                break;
            case $value > 20 && $value <= 30:
                return 30;
                break;
            case $value > 30 && $value <= 40:
                return 40;
                break;
            case $value > 40 && $value <= 50:
                return 50;
                break;
            case $value > 50 && $value <= 60:
                return 60;
                break;
            case $value > 60 && $value <= 70:
                return 70;
                break;
            case $value > 70 && $value <= 80:
                return 80;
                break;
            case $value > 80 && $value <= 90:
                return 90;
                break;
            case $value > 90:
                return 100;
                break;
            default:
                return 0;
                break;
        }
    }
}

if (!function_exists('starValue')) {
    function starValue($value)
    {
        switch (true) {
            case $value == 0:
                return 0;
                break;
            case $value <= 1:
                return 1;
                break;
            case $value <= 2:
                return 2;
                break;
            case $value <= 3:
                return 3;
                break;
            case $value <= 4:
                return 4;
                break;
            case $value <= 5:
                return 5;
                break;
            default:
                return 0;
                break;
        }
    }
}
